#include "httpserver.h"
#include "buffer.h"
#include "socket.h"

HttpServer::HttpServer(EventLoop *loop, const char *ip, uint16_t port)
	: loop(loop),
	  server(loop, ip, port, nullptr)
{
	server.setConnectionCallback(std::bind(&HttpServer::onConnection,
										   this, std::placeholders::_1));
	server.setMessageCallback(std::bind(&HttpServer::onMessage,
										this, std::placeholders::_1, std::placeholders::_2));
}

void HttpServer::setMessageCallback(HttpCallBack callback)
{
	httpCallback = callback;
}

HttpServer::~HttpServer()
{
}

void HttpServer::timerCallback(const std::thread::id &threadId)
{
	auto it = connectionBuckets.find(threadId);
	assert(it != connectionBuckets.end());
	it->second.push_back(Bucket());
}

void HttpServer::start()
{
	server.start();

	auto threadPool = server.getThreadPool();
	auto pools = threadPool->getAllLoops();

	for (int i = 0; i < pools.size(); i++)
	{
		WeakConnectionList buckets;
		buckets.resize(kIdleSecond);
		connectionBuckets[pools[i]->getThreadId()] = buckets;
		pools[i]->runAfter(kTimer, true, std::bind(&HttpServer::timerCallback, this, pools[i]->getThreadId()));
	}
}

void HttpServer::onConnection(const TcpConnectionPtr &conn)
{
	if (conn->connected())
	{
		Socket::setkeepAlive(conn->getSockfd(), kHeart);
		std::shared_ptr<HttpContext> c(new HttpContext());
		conn->setContext(c);

		auto it = connectionBuckets.find(conn->getLoop()->getThreadId());
		assert(it != connectionBuckets.end());

		EntryPtr entry(new Entry(conn));
		it->second.back().insert(entry);
		WeakEntryPtr weakEntry(entry);
		conn->setContext1(weakEntry);
	}
	else
	{
	}
}

void HttpServer::onMessage(const TcpConnectionPtr &conn, Buffer *buffer)
{
	if (buffer->readableBytes() >= kBuffer)
	{
		LOG_DEBUG << "";
		conn->shutdown();
		return;
	}

	std::shared_ptr<HttpContext> context = std::any_cast<std::shared_ptr<HttpContext>>(conn->getContext());
	if (!context->parseRequest(buffer))
	{
		conn->send("HTTP/1.1 400 Bad Request\r\n\r\n");
		conn->shutdown();
		LOG_DEBUG << "" << buffer->peek();
		return;
	}

	if (context->gotAll())
	{
		if (context->getRequest().getMethod() == HttpRequest::kPost)
		{
			context->getRequest().setQuery(buffer->peek(),
										   buffer->peek() + buffer->readableBytes());
		}

		httpCallback(conn, context->getRequest());
		context->reset();
	}
}
