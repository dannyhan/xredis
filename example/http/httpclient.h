#pragma once

#include "all.h"
#include "httpcontext.h"
#include "tcpclient.h"
#include "tcpconnection.h"
#include "timer.h"

class HttpClient
{
public:
	typedef std::function<void(const TcpConnectionPtr &,
							   HttpResponse &, const WeakTcpConnectionPtr &, const std::any &)>
		HttpCallBack;
	HttpClient(EventLoop *loop);

	void onConnection(const TcpConnectionPtr &conn);

	void onMessage(const TcpConnectionPtr &conn, Buffer *buffer);

	void postUrl(const char *ip, int16_t port, const std::string &url,
				 const std::string &body, const std::string &host, const std::string &type,
				 const TcpConnectionPtr &conn, const std::any &context, HttpCallBack &&callback);

	void getUrl(const char *ip, int16_t port, const std::string &url,
				const std::string &host, const TcpConnectionPtr &conn, const std::any &context, HttpCallBack &&callback);

	void timerCallback(const int64_t index);

private:
	EventLoop *loop;
	std::unordered_map<int64_t, TimerPtr> timers;
	std::unordered_map<int64_t, TcpClientPtr> tcpclients;
	std::unordered_map<int64_t, WeakTcpConnectionPtr> tcpConns;
	std::unordered_map<int64_t, HttpCallBack> httpCallbacks;
	std::unordered_map<int64_t, std::any> anyCallbacks;
	const static int32_t kHeart = 10;
	const static int32_t kTimer = 60;
	int64_t index;
};
