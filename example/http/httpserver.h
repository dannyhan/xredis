#pragma once

#include "all.h"
#include "httpcontext.h"
#include "tcpserver.h"
#include <boost/circular_buffer.hpp>
#include "tcpconnection.h"

class EventLoop;

class HttpServer
{
public:
	typedef std::function<void(const TcpConnectionPtr &, const HttpRequest &)> HttpCallBack;

	struct Entry
	{
		explicit Entry(const WeakTcpConnectionPtr &weakConn) : weakConn(weakConn)
		{
		}

		~Entry()
		{
			TcpConnectionPtr conn = weakConn.lock();
			if (conn)
			{
				conn->shutdown();
			}
		}

		WeakTcpConnectionPtr weakConn;
	};

	HttpServer(EventLoop *loop, const char *ip, uint16_t port);

	~HttpServer();

	void setThreadNum(int numThreads)
	{
		server.setThreadNum(numThreads);
	}

	TcpServer *getTcpServer() { return &server; }

	void setMessageCallback(HttpCallBack callback);

	void start();

	void timerCallback(const std::thread::id &threadId);

	void onConnection(const TcpConnectionPtr &conn);

	void onMessage(const TcpConnectionPtr &conn, Buffer *buffer);

	typedef std::shared_ptr<Entry> EntryPtr;
	typedef std::weak_ptr<Entry> WeakEntryPtr;
	typedef std::unordered_set<EntryPtr> Bucket;
	typedef boost::circular_buffer<Bucket> WeakConnectionList;

private:
	HttpServer(const HttpServer &);

	void operator=(const HttpServer &);

	EventLoop *loop;
	TcpServer server;
	HttpCallBack httpCallback;
	std::unordered_map<std::thread::id, WeakConnectionList> connectionBuckets;
	const static int32_t kHeart = 10;
	const static int32_t kTimer = 1;
	const static int32_t kIdleSecond = 60;
	const static int32_t kBuffer = 65536;
};
