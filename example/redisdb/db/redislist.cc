#include "redislist.h"

RedisList::RedisList(RedisDB* redis, const Options& options,
                     const std::string& path)
    : redis(redis), db(new DB(options, path)) {}

RedisList::~RedisList() {}

Status RedisList::Open() { return db->Open(); }

Status RedisList::DestroyDB(const std::string path, const Options& options) {
  return db->DestroyDB(path, options);
}

Status RedisList::LPush(const std::string_view& key,
                        const std::vector<std::string>& values, uint64_t* ret) {
  *ret = 0;
  ListsDataKey lkey(key, 0, 0);

  WriteBatch batch;
  HashLock l(&lockmgr, key);

  uint64_t index = 0;
  int32_t version = 0;
  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale() || plistsmetavalue.GetCount() == 0) {
      version = plistsmetavalue.InitialMetaValue();
    } else {
      version = plistsmetavalue.GetVersion();
    }

    for (const auto& value : values) {
      index = plistsmetavalue.GetLeftIndex();
      plistsmetavalue.ModifyLeftIndex(1);
      plistsmetavalue.ModifyCount(1);
      ListsDataKey listsdatakey(key, version, index);
      batch.Put(listsdatakey.Encode(), value);
    }

    batch.Put(lkey.Encode(), metavalue);
    *ret = plistsmetavalue.GetCount();
  } else if (s.IsNotFound()) {
    char str[8];
    EncodeFixed64(str, values.size());
    ListsMetaValue listsmetavalue(std::string_view(str, sizeof(uint64_t)));
    version = listsmetavalue.UpdateVersion();

    for (const auto& value : values) {
      index = listsmetavalue.GetLeftIndex();
      listsmetavalue.ModifyLeftIndex(1);
      ListsDataKey listsdatakey(key, version, index);
      batch.Put(listsdatakey.Encode(), value);
    }

    batch.Put(lkey.Encode(), listsmetavalue.Encode());
    *ret = listsmetavalue.GetRightIndex() - listsmetavalue.GetLeftIndex() - 1;
  } else {
    return s;
  }
  return db->Write(WriteOptions(), &batch);
}

Status RedisList::LPop(const std::string_view& key, std::string* element) {
  uint32_t statistic = 0;
  ListsDataKey lkey(key, 0, 0);

  WriteBatch batch;
  HashLock l(&lockmgr, key);
  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      int32_t version = plistsmetavalue.GetVersion();
      uint64_t firstnodeindex = plistsmetavalue.GetLeftIndex() + 1;
      ListsDataKey listsdatakey(key, version, firstnodeindex);
      s = db->Get(ReadOptions(), listsdatakey.Encode(), element);
      if (s.ok()) {
        batch.Delete(listsdatakey.Encode());
        statistic++;
        plistsmetavalue.ModifyCount(-1);
        plistsmetavalue.ModifyLeftIndex(-1);
        batch.Put(lkey.Encode(), metavalue);
        s = db->Write(WriteOptions(), &batch);
        return s;
      } else {
        return s;
      }
    }
  }
  return s;
}

Status RedisList::LRange(const std::string_view& key, int64_t start,
                         int64_t stop, std::vector<std::string>* ret) {
  ReadOptions readopts;
  std::shared_ptr<Snapshot> snapshot;
  SnapshotLock ss(db, snapshot);
  readopts.snapshot = snapshot;
  readopts.fillcache = false;

  ListsDataKey lkey(key, 0, 0);
  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      int32_t version = plistsmetavalue.GetVersion();
      uint64_t originleftindex = plistsmetavalue.GetLeftIndex() + 1;
      uint64_t originrightindex = plistsmetavalue.GetRightIndex() - 1;
      uint64_t sublistleftindex =
          start >= 0 ? originleftindex + start : originrightindex + start + 1;
      uint64_t sublistrightindex =
          stop >= 0 ? originleftindex + stop : originrightindex + stop + 1;

      if (sublistleftindex > sublistrightindex ||
          sublistleftindex > originrightindex ||
          sublistrightindex < originleftindex) {
        return Status::OK();
      } else {
        if (sublistleftindex < originleftindex) {
          sublistleftindex = originleftindex;
        }
        if (sublistrightindex > originrightindex) {
          sublistrightindex = originrightindex;
        }

        auto iter = db->NewIterator(ReadOptions());
        uint64_t currentindex = sublistleftindex;
        ListsDataKey startdatakey(key, version, currentindex);
        for (iter->Seek(startdatakey.Encode());
             iter->Valid() && currentindex <= sublistrightindex;
             iter->Next(), currentindex++) {
          ret->push_back(
              std::string(iter->value().data(), iter->value().size()));
        }
        return Status::OK();
      }
    }
  } else {
    return s;
  }
}

Status RedisList::LLen(const std::string_view& key, uint64_t* len) {
  *len = 0;
  std::string metavalue;
  ListsDataKey lkey(key, 0, 0);

  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      *len = plistsmetavalue.GetCount();
      return s;
    }
  }
  return s;
}

Status RedisList::LIndex(const std::string_view& key, int64_t index,
                         std::string* element) {
  ReadOptions readopts;
  std::shared_ptr<Snapshot> snapshot;
  SnapshotLock ss(db, snapshot);
  readopts.snapshot = snapshot;
  readopts.fillcache = false;

  std::string metavalue;
  ListsDataKey lkey(key, 0, 0);
  Status s = db->Get(readopts, lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    int32_t version = plistsmetavalue.GetVersion();
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      std::string tmpelement;
      uint64_t targetindex = index >= 0
                                 ? plistsmetavalue.GetLeftIndex() + index + 1
                                 : plistsmetavalue.GetRightIndex() + index;
      if (plistsmetavalue.GetLeftIndex() < targetindex &&
          targetindex < plistsmetavalue.GetRightIndex()) {
        ListsDataKey listsdatakey(key, version, targetindex);
        s = db->Get(readopts, listsdatakey.Encode(), &tmpelement);
        if (s.ok()) {
          *element = tmpelement;
        }
      } else {
        return Status::NotFound("");
      }
    }
  }
  return s;
}

Status RedisList::LInsert(const std::string_view& key,
                          const BeforeOrAfter& beforeorafter,
                          const std::string& pivot, const std::string& value,
                          int64_t* ret) {
  *ret = 0;
  ListsDataKey lkey(key, 0, 0);

  WriteBatch batch;
  HashLock l(&lockmgr, key);

  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      bool findpivot = false;
      uint64_t pivotindex = 0;
      uint32_t version = plistsmetavalue.GetVersion();
      uint64_t currentindex = plistsmetavalue.GetLeftIndex() + 1;
      auto iter = db->NewIterator(ReadOptions());
      ListsDataKey startdatakey(key, version, currentindex);

      for (iter->Seek(startdatakey.Encode());
           iter->Valid() && currentindex < plistsmetavalue.GetRightIndex();
           iter->Next(), currentindex++) {
        if (strcmp(iter->value().data(), pivot.data()) == 0) {
          findpivot = true;
          pivotindex = currentindex;
          break;
        }
      }

      if (!findpivot) {
        *ret = -1;
        return Status::NotFound("");
      } else {
        uint64_t targetindex;
        std::vector<std::string> listnodes;
        uint64_t midindex =
            plistsmetavalue.GetLeftIndex() +
            (plistsmetavalue.GetRightIndex() - plistsmetavalue.GetLeftIndex()) /
                2;
        if (pivotindex <= midindex) {
          targetindex = (beforeorafter == Before) ? pivotindex - 1 : pivotindex;

          currentindex = plistsmetavalue.GetLeftIndex() + 1;
          auto firsthalfiter = db->NewIterator(ReadOptions());
          ListsDataKey startdatakey(key, version, currentindex);
          for (firsthalfiter->Seek(startdatakey.Encode());
               firsthalfiter->Valid() && currentindex <= pivotindex;
               firsthalfiter->Next(), currentindex++) {
            if (currentindex == pivotindex) {
              if (beforeorafter == After) {
                listnodes.push_back(std::string(firsthalfiter->value().data(),
                                                firsthalfiter->value().size()));
              }
              break;
            }
            listnodes.push_back(std::string(firsthalfiter->value().data(),
                                            firsthalfiter->value().size()));
          }

          currentindex = plistsmetavalue.GetLeftIndex();
          for (const auto& node : listnodes) {
            ListsDataKey listsdatakey(key, version, currentindex++);
            batch.Put(listsdatakey.Encode(), node);
          }
          plistsmetavalue.ModifyLeftIndex(1);
        } else {
          targetindex = (beforeorafter == Before) ? pivotindex : pivotindex + 1;
          currentindex = pivotindex;
          auto afterhalfiter = db->NewIterator(ReadOptions());
          ListsDataKey startdatakey(key, version, currentindex);
          for (afterhalfiter->Seek(startdatakey.Encode());
               afterhalfiter->Valid() &&
               currentindex < plistsmetavalue.GetRightIndex();
               afterhalfiter->Next(), currentindex++) {
            if (currentindex == pivotindex &&
                beforeorafter == BeforeOrAfter::After) {
              continue;
            }
            listnodes.push_back(std::string(afterhalfiter->value().data(),
                                            afterhalfiter->value().size()));
          }

          currentindex = targetindex + 1;
          for (const auto& node : listnodes) {
            ListsDataKey listsdatakey(key, version, currentindex++);
            batch.Put(listsdatakey.Encode(), node);
          }
          plistsmetavalue.ModifyRightIndex(1);
        }

        plistsmetavalue.ModifyCount(1);
        batch.Put(lkey.Encode(), metavalue);
        ListsDataKey liststargetkey(key, version, targetindex);
        batch.Put(liststargetkey.Encode(), value);
        *ret = plistsmetavalue.GetCount();
        return db->Write(WriteOptions(), &batch);
      }
    }
  } else if (s.IsNotFound()) {
    *ret = 0;
  }
  return s;
}

Status RedisList::LRem(const std::string_view& key, int64_t count,
                       const std::string_view& value, uint64_t* ret) {
  *ret = 0;
  WriteBatch batch;
  ListsDataKey lkey(key, 0, 0);

  HashLock l(&lockmgr, key);
  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      uint64_t currentindex;
      std::vector<uint64_t> targetindex;
      std::vector<uint64_t> deleteindex;
      uint64_t rest = (count < 0) ? -count : count;
      uint32_t version = plistsmetavalue.GetVersion();
      uint64_t startindex = plistsmetavalue.GetLeftIndex() + 1;
      uint64_t stopindex = plistsmetavalue.GetRightIndex() - 1;
      ListsDataKey startdatakey(key, version, startindex);
      ListsDataKey stopdatakey(key, version, stopindex);
      if (count >= 0) {
        currentindex = startindex;
        std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
        for (iter->Seek(startdatakey.Encode());
             iter->Valid() && currentindex <= stopindex &&
             (!count || rest != 0);
             iter->Next(), currentindex++) {
          if (strcmp(iter->value().data(), value.data()) == 0) {
            targetindex.push_back(currentindex);
            if (count != 0) {
              rest--;
            }
          }
        }
      } else {
        currentindex = stopindex;
        std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
        for (iter->Seek(stopdatakey.Encode());
             iter->Valid() && currentindex >= startindex &&
             (!count || rest != 0);
             iter->Prev(), currentindex--) {
          if (strcmp(iter->value().data(), value.data()) == 0) {
            targetindex.push_back(currentindex);
            if (count != 0) {
              rest--;
            }
          }
        }
      }
      if (targetindex.empty()) {
        *ret = 0;
        return Status::NotFound("");
      } else {
        rest = targetindex.size();
        uint64_t sublistleftindex =
            (count >= 0) ? targetindex[0] : targetindex[targetindex.size() - 1];
        uint64_t sublistrightindex =
            (count >= 0) ? targetindex[targetindex.size() - 1] : targetindex[0];
        uint64_t leftpartlen = sublistrightindex - startindex;
        uint64_t rightpartlen = stopindex - sublistleftindex;
        if (leftpartlen <= rightpartlen) {
          uint64_t left = sublistrightindex;
          currentindex = sublistrightindex;
          ListsDataKey sublistrightkey(key, version, sublistrightindex);
          std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
          for (iter->Seek(sublistrightkey.Encode());
               iter->Valid() && currentindex >= startindex;
               iter->Prev(), currentindex--) {
            if (!strcmp(iter->value().data(), value.data()) && rest > 0) {
              rest--;
            } else {
              ListsDataKey listsdatakey(key, version, left--);
              batch.Put(listsdatakey.Encode(), iter->value());
            }
          }

          uint64_t getleftIndex = plistsmetavalue.GetLeftIndex();
          for (uint64_t idx = 0; idx < targetindex.size(); ++idx) {
            deleteindex.push_back(getleftIndex + idx + 1);
          }
          plistsmetavalue.ModifyLeftIndex(-targetindex.size());
        } else {
          uint64_t right = sublistleftindex;
          currentindex = sublistleftindex;
          ListsDataKey sublistleftkey(key, version, sublistleftindex);
          std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
          for (iter->Seek(sublistleftkey.Encode());
               iter->Valid() && currentindex <= stopindex;
               iter->Next(), currentindex++) {
            if (!strcmp(iter->value().data(), value.data()) && rest > 0) {
              rest--;
            } else {
              ListsDataKey listsdatakey(key, version, right++);
              batch.Put(listsdatakey.Encode(), iter->value());
            }
          }

          uint64_t GetRightIndex = plistsmetavalue.GetRightIndex();
          for (uint64_t idx = 0; idx < targetindex.size(); ++idx) {
            deleteindex.push_back(GetRightIndex - idx - 1);
          }
          plistsmetavalue.ModifyRightIndex(-targetindex.size());
        }

        plistsmetavalue.ModifyCount(-targetindex.size());
        batch.Put(key, metavalue);
        for (const auto& idx : deleteindex) {
          ListsDataKey listsdatakey(key, version, idx);
          batch.Delete(listsdatakey.Encode());
        }
        *ret = targetindex.size();
        return db->Write(WriteOptions(), &batch);
      }
    }
  } else if (s.IsNotFound()) {
    *ret = 0;
  }
  return s;
}

Status RedisList::LSet(const std::string_view& key, int64_t index,
                       const std::string_view& value) {}

Status RedisList::LTrim(const std::string_view& key, int64_t start,
                        int64_t stop) {}

Status RedisList::RPush(const std::string_view& key,
                        const std::vector<std::string>& values, uint64_t* ret) {
  *ret = 0;
  ListsDataKey lkey(key, 0, 0);
  WriteBatch batch;

  uint64_t index = 0;
  int32_t version = 0;
  std::string metavalue;

  HashLock l(&lockmgr, key);
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale() || plistsmetavalue.GetCount() == 0) {
      version = plistsmetavalue.InitialMetaValue();
    } else {
      version = plistsmetavalue.GetVersion();
    }

    for (const auto& value : values) {
      index = plistsmetavalue.GetRightIndex();
      plistsmetavalue.ModifyRightIndex(1);
      plistsmetavalue.ModifyCount(1);
      ListsDataKey listsdatakey(key, version, index);
      batch.Put(listsdatakey.Encode(), value);
    }

    batch.Put(lkey.Encode(), metavalue);
    *ret = plistsmetavalue.GetCount();
  } else if (s.IsNotFound()) {
    char str[8];
    EncodeFixed64(str, values.size());
    ListsMetaValue listsmetavalue(std::string_view(str, sizeof(uint64_t)));
    version = listsmetavalue.UpdateVersion();
    for (auto value : values) {
      index = listsmetavalue.GetRightIndex();
      listsmetavalue.ModifyRightIndex(1);
      ListsDataKey listsdatakey(key, version, index);
      batch.Put(listsdatakey.Encode(), value);
    }

    batch.Put(lkey.Encode(), listsmetavalue.Encode());
    *ret = listsmetavalue.GetRightIndex() - listsmetavalue.GetLeftIndex() - 1;
  } else {
    return s;
  }
  return db->Write(WriteOptions(), &batch);
}

Status RedisList::RPop(const std::string_view& key, std::string* element) {
  uint32_t statistic = 0;
  WriteBatch batch;
  ListsDataKey lkey(key, 0, 0);

  HashLock l(&lockmgr, key);
  std::string metavalue;
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      int32_t version = plistsmetavalue.GetVersion();
      uint64_t lastnodeindex = plistsmetavalue.GetRightIndex() - 1;
      ListsDataKey listsdatakey(key, version, lastnodeindex);
      s = db->Get(ReadOptions(), listsdatakey.Encode(), element);
      if (s.ok()) {
        batch.Delete(listsdatakey.Encode());
        plistsmetavalue.ModifyCount(-1);
        plistsmetavalue.ModifyRightIndex(-1);
        batch.Put(lkey.Encode(), metavalue);
        return db->Write(WriteOptions(), &batch);
      } else {
        return s;
      }
    }
  }
  return s;
}

Status RedisList::ScanKeyNum(KeyInfo* keyinfo) {
  uint64_t keys = 0;
  uint64_t expires = 0;
  uint64_t ttlsum = 0;
  uint64_t invaildkeys = 0;

  std::string key;
  ReadOptions iteropts;
  std::shared_ptr<Snapshot> snapshot;
  SnapshotLock ss(db, snapshot);
  iteropts.snapshot = snapshot;
  iteropts.fillcache = false;
  int64_t curtime = time(0);

  ListsDataKey lkey(key, 0, 0);
  std::shared_ptr<Iterator> iter = db->NewIterator(iteropts);
  for (iter->Seek(lkey.Encode()); iter->Valid(); iter->Next()) {
    ParsedListsDataKey plistsdatakey(iter->key());
    if (plistsdatakey.GetVersion() != 0) {
      break;
    }

    ParsedListsMetaValue plistsmetavalue(iter->value());
    if (plistsmetavalue.IsStale()) {
      invaildkeys++;
    } else {
      if (!plistsmetavalue.IsPermanentSurvival()) {
        expires++;
        ttlsum += plistsmetavalue.GetTimestamp() - curtime;
      }
    }
  }

  keyinfo->keys = keys;
  keyinfo->expires = expires;
  keyinfo->avgttl = (expires != 0) ? ttlsum / expires : 0;
  keyinfo->invaildkeys = invaildkeys;
  return Status::OK();
}

Status RedisList::ScanKeys(const std::string& pattern,
                           std::vector<std::string>* keys) {
  std::string key;
  ReadOptions iteropts;
  std::shared_ptr<Snapshot> snapshot;
  SnapshotLock ss(db, snapshot);
  iteropts.snapshot = snapshot;
  iteropts.fillcache = false;

  ListsDataKey lkey(key, 0, 0);
  std::shared_ptr<Iterator> iter = db->NewIterator(iteropts);
  for (iter->Seek(lkey.Encode()); iter->Valid(); iter->Next()) {
    ParsedListsDataKey plistsdatakey(iter->key());
    if (plistsdatakey.GetVersion() != 0) {
      break;
    }

    ParsedListsMetaValue plistsmetavalue(iter->value());
    if (!plistsmetavalue.IsStale() && plistsmetavalue.GetCount() != 0) {
      key = ToString(iter->key());
      if (StringMatchLen(pattern.data(), pattern.size(), key.data(), key.size(),
                         0)) {
        keys->push_back(key);
      }
    }
  }
  return Status::OK();
}

Status RedisList::Expire(const std::string_view& key, int32_t ttl) {
  std::string metavalue;
  ListsDataKey lkey(key, 0, 0);

  HashLock l(&lockmgr, key);
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    }

    if (ttl > 0) {
      plistsmetavalue.SetRelativeTimestamp(ttl);
      s = db->Put(WriteOptions(), lkey.Encode(), metavalue);
    } else {
      plistsmetavalue.InitialMetaValue();
      s = db->Put(WriteOptions(), lkey.Encode(), metavalue);
    }
  }
  return s;
}

Status RedisList::Del(const std::string_view& key) {
  ListsDataKey lkey(key, 0, 0);
  std::string metavalue;
  WriteBatch batch;

  HashLock l(&lockmgr, key);
  Status s = db->Get(ReadOptions(), lkey.Encode(), &metavalue);
  if (s.ok()) {
    ParsedListsMetaValue plistsmetavalue(&metavalue);
    if (plistsmetavalue.IsStale()) {
      return Status::NotFound("Stale");
    } else if (plistsmetavalue.GetCount() == 0) {
      return Status::NotFound("");
    } else {
      batch.Delete(lkey.Encode());
      int64_t curindex = plistsmetavalue.GetLeftIndex();
      int64_t stopindex = plistsmetavalue.GetCount() + curindex;
      int32_t version = plistsmetavalue.GetVersion();
      ListsDataKey ldatakey(key, version, curindex);
      std::string_view prefix = ldatakey.Encode();
      auto iter = db->NewIterator(ReadOptions());
      for (iter->Seek(prefix); iter->Valid() && curindex < stopindex;
           iter->Next(), curindex++) {
        batch.Delete(iter->key());
      }
      return db->Write(WriteOptions(), &batch);
    }
  }
  return s;
}