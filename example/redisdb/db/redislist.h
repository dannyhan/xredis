#pragma once
#include <chrono>
#include <memory>
#include <ratio>
#include <string>
#include <string_view>

#include "coding.h"
#include "db.h"
#include "lockmgr.h"
#include "option.h"
#include "redis.h"
#include "serialize.h"

class RedisDB;

class RedisList {
 public:
  RedisList(RedisDB* redis, const Options& options, const std::string& path);
  ~RedisList();

  Status Open();

  Status DestroyDB(const std::string path, const Options& options);

  Status LPop(const std::string_view& key, std::string* element);

  Status LPush(const std::string_view& key,
               const std::vector<std::string>& values, uint64_t* ret);

  Status LRange(const std::string_view& key, int64_t start, int64_t stop,
                std::vector<std::string>* ret);

  Status LLen(const std::string_view& key, uint64_t* len);

  Status LIndex(const std::string_view& key, int64_t index,
                std::string* element);

  Status LInsert(const std::string_view& key,
                 const BeforeOrAfter& beforeorafter, const std::string& pivot,
                 const std::string& value, int64_t* ret);

  Status LRem(const std::string_view& key, int64_t count,
              const std::string_view& value, uint64_t* ret);

  Status LSet(const std::string_view& key, int64_t index,
              const std::string_view& value);

  Status LTrim(const std::string_view& key, int64_t start, int64_t stop);

  Status RPush(const std::string_view& key,
               const std::vector<std::string>& values, uint64_t* ret);

  Status RPop(const std::string_view& key, std::string* element);

  Status ScanKeyNum(KeyInfo* keyinfo);

  Status ScanKeys(const std::string& pattern, std::vector<std::string>* keys);

  Status Del(const std::string_view& key);

  Status Expire(const std::string_view& key, int32_t ttl);

 private:
  RedisDB* redis;
  std::shared_ptr<DB> db;
  LockMgr lockmgr;
};