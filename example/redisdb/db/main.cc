#include "redisdb.h"

bool CHECK_ELEMENTS_MATCH(RedisDB *const db, const std::string_view key,
	const std::vector<std::string> expectelements) {
	std::vector<std::string> elementsout;
	Status s = db->LRange(key, 0, -1, &elementsout);
	if (!s.ok() && !s.IsNotFound()) {
		return false;
	}
	
	if (elementsout.size() != expectelements.size()) {
		return false;
	}
	
	if (s.IsNotFound() && expectelements.empty()) {
		return true;
	}
	
	for (uint64_t idx = 0; idx < elementsout.size(); ++idx) {
		if (strcmp(elementsout[idx].c_str(), expectelements[idx].c_str())) {
			return false;
		}
	}
	return true;	
}

bool CHECK_ELEMENTS_MATCH(const std::vector<std::string>& elementsout,
                           const std::vector<std::string>& expectelements) {
	if (elementsout.size() != expectelements.size()) {
		return false;
	}
	
	for (uint64_t idx = 0; idx < elementsout.size(); ++idx) {
		if (strcmp(elementsout[idx].c_str(), expectelements[idx].c_str())) {
			return false;
		}
	}
	return true;
}

bool CHECK_LEN_MATCH(RedisDB *const db,
                      const std::string_view& key,
                      uint64_t expectlen) {
	uint64_t len = 0;
	Status s = db->LLen(key, &len);
	if (!s.ok() && !s.IsNotFound()) {
		return false;
	}
	
	if (s.IsNotFound() && !expectlen) {
		return true;
	}
	return len == expectlen;
}

bool CHECK_MAKE_EXPIRED(RedisDB *const db,
                         const std::string_view& key) {
	std::map<DataType, Status> typestatus;
	int ret = db->Expire(key, 1, &typestatus);
	if (!ret || !typestatus[DataType::kLists].ok()) {
		return false;
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	return true;
}


int main() {
	Options opts;
	opts.createifmissing = true;
	RedisDB db(opts, "./test_list");
	Status s = db.Open();
	if (s.ok()) {
		printf("Open success\n");
	}
	else {
		printf("Open failed, error: %s\n", s.ToString().c_str());
		return -1;
	}
	
	uint64_t num;
	std::string element;
	
	std::vector<std::string> test1{"n", "i", "l", "p", "p", "e", "z"};
	s = db.LPush("TEST1_LINDEX_KEY", test1, &num);
	assert(s.ok());
	assert(test1.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST1_LINDEX_KEY", test1.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST1_LINDEX_KEY", {"z", "e", "p", "p", "l", "i", "n"}));
	
	s = db.LIndex("TEST1_LINDEX_KEY", 0, &element);
	assert(s.ok());
	assert(element == "z");

	s = db.LIndex("TEST1_LINDEX_KEY", 4, &element);
	assert(s.ok());
	assert(element == "l");

	s = db.LIndex("TEST1_LINDEX_KEY", 6, &element);
	assert(s.ok());
	assert(element == "n");

	s = db.LIndex("TEST1_LINDEX_KEY", 10, &element);
	assert(s.IsNotFound());

	s = db.LIndex("TEST1_LINDEX_KEY", -1, &element);
	assert(s.ok());
	assert(element == "n");

	s = db.LIndex("TEST1_LINDEX_KEY", -4, &element);
	assert(s.ok());
	assert(element == "p");

	s = db.LIndex("TEST1_LINDEX_KEY", -7, &element);
	assert(s.ok());
	assert(element == "z");

	s = db.LIndex("TEST1_LINDEX_KEY", -10000, &element);
	assert(s.IsNotFound());
	
	
	std::vector<std::string> test2 {"b", "a", "t", "t", "l", "e"};
	s = db.RPush("TEST2_LINDEX_KEY", test2, &num);
	assert(s.ok());
	assert(test2.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST2_LINDEX_KEY", test2.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST2_LINDEX_KEY", {"b", "a", "t", "t", "l", "e"}));

	assert(CHECK_MAKE_EXPIRED(&db, "TEST2_LINDEX_KEY"));
	assert(CHECK_LEN_MATCH(&db, "TEST2_LINDEX_KEY", 0));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST2_LINDEX_KEY", {}));
	s = db.LIndex("TEST2_LINDEX_KEY", 0, &element);
	assert(s.IsNotFound());
  
  
	std::vector<std::string> test3{"m", "i", "s", "t", "y"};
	s = db.RPush("TEST3_LINDEX_KEY", test3, &num);
	assert(s.ok());
	assert(test3.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST3_LINDEX_KEY", test3.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST3_LINDEX_KEY", {"m", "i", "s", "t", "y"}));

	std::vector<std::string> delkeys = {"TEST3_LINDEX_KEY"};
	std::map<DataType, Status> typestatus;
	db.Del(delkeys, &typestatus);
	assert(typestatus[DataType::kLists].ok());
	assert(CHECK_LEN_MATCH(&db, "TEST3_LINDEX_KEY", 0));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST3_LINDEX_KEY", {}));

	s = db.LIndex("TEST3_LINDEX_KEY", 0, &element);
	assert(s.IsNotFound());
	
	s = db.LIndex("TEST4_LINDEX_KEY", 0, &element);
	assert(s.IsNotFound());
	
	std::vector<std::string> test5{"m", "i", "s", "t", "y"};
	s = db.RPush("TEST5_LINDEX_KEY", test5, &num);
	assert(s.ok());
	assert(test5.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST5_LINDEX_KEY", test5.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST5_LINDEX_KEY", {"m", "i", "s", "t", "y"}));

	s = db.LPop("TEST5_LINDEX_KEY", &element);
	assert(s.ok());
	assert(element == "m");

	s = db.LIndex("TEST5_LINDEX_KEY", -5, &element);
	assert(s.IsNotFound());
	
	std::vector<std::string> test6{"m", "i", "s", "t", "y"};
	s = db.RPush("TEST6_LINDEX_KEY", test6, &num);
	assert(s.ok());
	assert(test6.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST6_LINDEX_KEY", test6.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST6_LINDEX_KEY", {"m", "i", "s", "t", "y"}));

	s = db.RPop("TEST6_LINDEX_KEY", &element);
	assert(s.ok());
	assert(element == "y");

	s = db.LIndex("TEST6_LINDEX_KEY", 4, &element);
	assert(s.IsNotFound());
	
	std::vector<std::string> test7{"m", "i", "s", "t", "y"};
	s = db.RPush("TEST7_LINDEX_KEY", test7, &num);
	assert(s.ok());
	assert(test7.size() == num);
	assert(CHECK_LEN_MATCH(&db, "TEST7_LINDEX_KEY", test7.size()));
	assert(CHECK_ELEMENTS_MATCH(&db, "TEST7_LINDEX_KEY", {"m", "i", "s", "t", "y"}));

	s = db.LTrim("TEST7_LINDEX_KEY", 1, 3);
	assert(s.ok());

	s = db.LIndex("TEST7_LINDEX_KEY", 3, &element);
	assert(s.IsNotFound());

	s = db.LIndex("TEST7_LINDEX_KEY", -4, &element);
	assert(s.IsNotFound());
  
	return 0;
}
