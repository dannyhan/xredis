#include <stdint.h>
#include <stdio.h>

#include <algorithm>
#include <set>
#include <string>
#include <vector>
#include <assert.h>
#include <random>

#include "db.h"
#include "filename.h"
#include "logwriter.h"
#include "logreader.h"
#include "tablecache.h"
#include "versionset.h"
#include "status.h"
#include "table.h"
#include "tablebuilder.h"
#include "block.h"
#include "merger.h"
#include "option.h"
#include "logging.h"
#include "env.h"

const int kNumNonTableCacheFiles = 10;

std::default_random_engine e;

std::string MakeKey(unsigned int num) {
	char buf[30];
	snprintf(buf, sizeof(buf), "%016u", num);
	return std::string(buf);
}

class AtomicCounter {
public:
	AtomicCounter() : count(0) { }
	void Increment() {
		Incrementby(1);
	}

	void Incrementby(int count) {
		std::unique_lock<std::mutex> lck(mutex);
		this->count += count;
	}

	int read() {
		std::unique_lock<std::mutex> lck(mutex);
		return count;
	}

	void Reset() {
		std::unique_lock<std::mutex> lck(mutex);
		count = 0;
	}
private:
	std::mutex mutex;
	int count;
};

// Special Env used to delay background operations.
class SpecialEnv {
public:
	// sstable/log Sync() calls are blocked while this pointer is non-null.
	std::atomic<bool> delaydatasync;

	// sstable/log Sync() calls return an error.
	std::atomic<bool> datasyncerror;

	// Simulate no-space errors while this pointer is non-null.
	std::atomic<bool> nospace;

	// Simulate non-writable file system while this pointer is non-null.
	std::atomic<bool> nonwriteable;

	// Force sync of manifest files to fail while this pointer is non-null.
	std::atomic<bool> nonmanifestsyncerror;

	// Force Write to manifest files to fail while this pointer is non-null.
	std::atomic<bool> manifestwriteerror;

	bool manifecountrandomreads;
	AtomicCounter randomreadcounter;

	explicit SpecialEnv()
		:delaydatasync(false),
		datasyncerror(false),
		nospace(false),
		nonwriteable(false),
		nonmanifestsyncerror(false),
		manifestwriteerror(false),
		manifecountrandomreads(false) {
	}
};

class DBTest {
private:
	// Sequence of option configurations to try
	enum OptionConfig {
		kDefault,
		kReuse,
		kFilter,
		kUncompressed,
		kEnd
	};
	int optionconfig;

public:
	SpecialEnv senv;
	std::string dbname;
	std::shared_ptr<DB> db;
	Options lastOptions;
	Env env;
	std::mutex mutex;

	DBTest() : optionconfig(kDefault) {
		dbname = "db_test";
		Reopen();
	}

	~DBTest() {
		db->DestroyDB(dbname, Options());
	}

	void Reopen(Options* options = nullptr) {
		Status s = TryReopen(options);
		assert(s.ok());
	}

	void DestroyAndReopen(Options* options = nullptr) {
		Status s = TryReopen(options);
		assert(s.ok());
	}

	Status TryReopen(Options* options) {
		Options opts;
		if (options != nullptr) {
			opts = *options;
		}
		else {
			opts = CurrentOptions();
			opts.createifmissing = true;
		}

		lastOptions = opts;
		db.reset(new DB(opts, dbname));
		db->DestroyDB(dbname, Options());
		Status s = db->Open();
		return s;
	}

	// Switch to a fresh database with the Next option configuration to
	// test.  Return false if there are no more configurations to test.
	bool ChangeOptions() {
		optionconfig++;
		if (optionconfig >= kEnd) {
			return false;
		}
		else {
			DestroyAndReopen();
			return true;
		}
	}

	// Return the current option configuration.
	Options CurrentOptions() {
		Options options;
		options.reuselogs = false;
		switch (optionconfig) {
		case kReuse:
			options.reuselogs = true;
			break;
		case kFilter:
			break;
		case kUncompressed:
			options.compression = kNoCompression;
			break;
		default:
			break;
		}
		return options;
	}

	Status Put(const std::string& k, const std::string& v) {
		return db->Put(WriteOptions(), k, v);
	}

	Status Delete(const std::string& k) {
		return db->Delete(WriteOptions(), k);
	}

	std::string Get(const std::string& k) {
		ReadOptions options;
		//options.snapshot = snapshot;
		std::string result;
		Status s = db->Get(ReadOptions(), k, &result);
		if (s.IsNotFound()) {
			result = "NOT_FOUND";
		}
		else if (!s.ok()) {
			result = s.ToString();
		}
		return result;
	}

	std::string IterStatus(const std::shared_ptr <Iterator>& iter) {
		std::string result;
		if (iter->Valid()) {
			std::string key = std::string(iter->key().data(), iter->key().size());
			std::string value = std::string(iter->value().data(), iter->value().size());
			result = key + "->" + value;
		}
		else {
			result = "(invalid)";
		}
		return result;
	}

	// Return a string that Contains all key,value pairs in order,
	// formatted like "(k1->v1)(k2->v2)".
	std::string Contents() {
		std::vector<std::string> forward;
		std::string result;
		std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
		for (iter->SeekToFirst(); iter->Valid(); iter->Next()) {
			std::string s = IterStatus(iter);
			result.push_back('(');
			result.append(s);
			result.push_back(')');
			forward.push_back(s);
		}

		// Check reverse iteration results are the reverse of forward results
		size_t matched = 0;
		for (iter->SeekToLast(); iter->Valid(); iter->Prev()) {
			assert(matched < forward.size());
			assert(IterStatus(iter) == forward[forward.size() - matched - 1]);
			matched++;
		}

		assert(matched == forward.size());
		return result;
	}

	std::string AllEntriesFor(const std::string_view& userkey) {
		std::shared_ptr<Iterator> iter = db->TESTNewInternalIterator();
		InternalKey target(userkey, kMaxSequenceNumber, kTypeValue);
		iter->Seek(target.Encode());
		std::string result;
		if (!iter->status().ok()) {
			result = iter->status().ToString();
		}
		else {
			result = "[ ";
			bool first = true;
			while (iter->Valid()) {
				ParsedInternalKey ikey;
				if (!ParseInternalKey(iter->key(), &ikey)) {
					result += "CORRUPTED";
				}
				else {
					if (lastOptions.comparator->Compare(ikey.userkey, userkey) != 0) {
						break;
					}
					if (!first) {
						result += ", ";
					}
					first = false;
					switch (ikey.type) {
					case kTypeValue:
						result += iter->value();
						break;
					case kTypeDeletion:
						result += "DEL";
						break;
					}
				}
				iter->Next();
			}
			if (!first) {
				result += " ";
			}
			result += "]";
		}
		return result;
	}

	int NumTableFilesAtLevel(int level) {
		std::string property;
		assert(db->GetProperty("leveldb.num-files-at-level" + NumberToString(level), &property));
		return std::stoi(property);
	}

	int TotalTableFiles() {
		int result = 0;
		for (int level = 0; level < kNumLevels; level++) {
			result += NumTableFilesAtLevel(level);
		}
		return result;
	}

	// Return spread of files per level
	std::string FilesPerLevel() {
		std::string result;
		int lastOffest = 0;
		for (int level = 0; level < kNumLevels; level++) {
			int f = NumTableFilesAtLevel(level);
			char buf[100];
			snprintf(buf, sizeof(buf), "%s%d", (level ? "," : ""), f);
			result += buf;
			if (f > 0) {
				lastOffest = result.size();
			}
		}
		result.resize(lastOffest);
		return result;
	}

	int CountFiles() {
		std::vector <std::string> files;
		env.GetChildren(dbname, &files);
		return static_cast<int>(files.size());
	}

	uint64_t size(const std::string_view& start, const std::string_view& limit) {
		Range r(start, limit);
		uint64_t size;
		db->GetApproximateSizes(&r, 1, &size);
		return size;
	}

	void Compact(const std::string_view& start, const std::string_view& limit) {
		db->CompactRange(&start, &limit);
	}

	// Do n memtable compactions, each of which produces an sstable
	// covering the range [small_key,large_key].
	void MakeTables(int n, const std::string & smallKey,
		const std::string & largeKey) {
		for (int i = 0; i < n; i++) {
			Put(smallKey, "begin");
			Put(largeKey, "end");
			db->TESTCompactMemTable();
		}
	}

	// Prevent pushing of new sstables into deeper levels by adding
	// tables that cover a specified range to all levels.
	void FillLevels(const std::string& smallest, const std::string& largest) {
		MakeTables(kNumLevels, smallest, largest);
	}

	void DumpFileCounts(const char* label) {
		fprintf(stderr, "---\n%s:\n", label);
		fprintf(stderr, "maxoverlap: %lld\n",
			static_cast<long long>(
				db->TESTMaxNextLevelOverlappingBytes()));
		for (int level = 0; level < kNumLevels; level++) {
			int num = NumTableFilesAtLevel(level);
			if (num > 0) {
				fprintf(stderr, "  level %3d : %d files\n", level, num);
			}
		}
	}

	std::string DumpSSTableList() {
		std::string property;
		db->GetProperty("leveldb.sstables", &property);
		return property;
	}

	bool DeleteAnSSTFile() {
		std::vector <std::string> filenames;
		assert(env.GetChildren(dbname, &filenames).ok());
		uint64_t number;
		FileType type;
		for (size_t i = 0; i < filenames.size(); i++) {
			if (ParseFileName(filenames[i], &number, &type) && type == kTableFile) {
				assert(env.DeleteFile(TableFileName(dbname, number)).ok());
				return true;
			}
		}
		return false;
	}

	// Returns number of files renamed.
	int RenameLDBToSST() {
		std::vector <std::string> filenames;
		assert(env.GetChildren(dbname, &filenames).ok());
		uint64_t number;
		FileType type;
		int filesRenamed = 0;
		for (size_t i = 0; i < filenames.size(); i++) {
			if (ParseFileName(filenames[i], &number, &type) && type == kTableFile) {
				const std::string from = TableFileName(dbname, number);
				const std::string to = SSTableFileName(dbname, number);
				assert(env.RenameFile(from, to).ok());
				filesRenamed++;
			}
		}
		return filesRenamed;
	}

	void Empty() {
		do {
			assert(db != nullptr);
			assert("NOT_FOUND" == Get("foo"));
		} while (ChangeOptions());
	}

	void EmptyKey() {
		do {
			assert(Put("", "v1").ok());
			assert("v1" == Get(""));
			assert(Put("", "v2").ok());
			assert("v2" == Get(""));
		} while (ChangeOptions());
	}

	void EmptyValue() {
		do {
			assert(Put("key", "v1").ok());
			assert("v1" == Get("key"));
			assert(Put("key", "").ok());
			assert("" == Get("key"));
			assert(Put("key", "v2").ok());
			assert("v2" == Get("key"));
		} while (ChangeOptions());
	}

	void ReadWrite() {
		do {
			assert(Put("foo", "v1").ok());
			assert("v1" == Get("foo"));
			assert(Put("bar", "v2").ok());
			assert(Put("foo", "v3").ok());
			assert("v3" == Get("foo"));
			assert("v2" == Get("bar"));
		} while (ChangeOptions());
	}

	void PutDeleteGet() {
		do {
			assert(db->Put(WriteOptions(), "foo", "v1").ok());
			assert("v1" == Get("foo"));
			assert(db->Put(WriteOptions(), "foo", "v2").ok());
			assert("v2" == Get("foo"));
			assert(db->Delete(WriteOptions(), "foo").ok());
			assert("NOT_FOUND" == Get("foo"));
		} while (ChangeOptions());
	}

	void GetFromImmutableLayer() {
		do {
			Options options = CurrentOptions();
			options.writebuffersize = 100000;  // Small Write buffer
			options.createifmissing = true;
			Reopen(&options);

			assert(Put("foo", "v1").ok());
			assert("v1" == Get("foo"));

			// Block sync calls.
			senv.delaydatasync.store(true, std::memory_order_release);
			Put("k1", std::string(100000, 'x'));             // Fill memtable.
			Put("k2", std::string(100000, 'y'));             // Trigger compaction.
			assert("v1" == Get("foo"));
			// Release sync calls.
			senv.delaydatasync.store(false, std::memory_order_release);
		} while (ChangeOptions());
	}

	void GetFromVersions() {
		do {
			assert(Put("foo", "v1").ok());
			db->TESTCompactMemTable();
			assert("v1" == Get("foo"));
		} while (ChangeOptions());
	}

	void GetMemUsage() {
		do {
			assert(Put("foo", "v1").ok());
			std::string val;
			assert(db->GetProperty("leveldb.approximate-memory-usage", &val));
			int usage = std::stoi(val);
			assert(usage > 0);
			assert(usage < 5 * 1024 * 1024);
		} while (ChangeOptions());
	}

	void GetLevel0Ordering() {
		do {
			// Check that we process level-0 files in correct order.  The code
			// below generates two level-0 files where the earlier one comes
			// before the later one in the level-0 file list since the earlier
			// one has a smaller "smallest" key.
			assert(Put("bar", "b").ok());
			assert(Put("foo", "v1").ok());
			db->TESTCompactMemTable();
			assert(Put("foo", "v2").ok());
			db->TESTCompactMemTable();
			assert("v2" == Get("foo"));
		} while (ChangeOptions());
	}

	void GetOrderedByLevels() {
		do {
			assert(Put("foo", "v1").ok());
			Compact("a", "z");
			assert("v1" == Get("foo"));
			assert(Put("foo", "v2").ok());
			assert("v2" == Get("foo"));
			db->TESTCompactMemTable();
			assert("v2" == Get("foo"));
		} while (ChangeOptions());
	}
};

void BMLogAndApply(int iters, int numbasefiles) {
	Options opts;
	opts.createifmissing = true;
	std::string dbname = "./leveldb_test_benchmark";
	std::shared_ptr <Env> env(new	Env());
	DB db(opts, dbname);
	db.DestroyDB(dbname, opts);

	std::mutex mutex;
	std::unique_lock<std::mutex> lck(mutex);
	Status s = db.Open();
	assert(s.ok());

	BytewiseComparatorImpl byteImpl;
	InternalKeyComparator cmp(&byteImpl);
	Options options;
	VersionSet vset(dbname, options, nullptr, &cmp);
	bool manifest;
	s = vset.Recover(&manifest);
	assert(s.ok());

	VersionEdit vbase;
	uint64_t fnum = 1;
	for (int i = 0; i < numbasefiles; i++) {
		InternalKey start(MakeKey(2 * fnum), 1, kTypeValue);
		InternalKey limit(MakeKey(2 * fnum + 1), 1, kTypeDeletion);
		vbase.AddFile(2, fnum++, 1 /* file size */, start, limit);
	}

	assert(vset.LogAndApply(&vbase, &mutex).ok());
	uint64_t startMicros = env->NowMicros();

	for (int i = 0; i < iters; i++) {
		VersionEdit vedit;
		vedit.DeleteFile(2, fnum);
		InternalKey start(MakeKey(2 * fnum), 1, kTypeValue);
		InternalKey limit(MakeKey(2 * fnum + 1), 1, kTypeDeletion);
		vedit.AddFile(2, fnum++, 1 /* file size */, start, limit);
		vset.LogAndApply(&vedit, &mutex);
	}

	uint64_t stopMicros = env->NowMicros();
	unsigned int us = stopMicros - startMicros;
	char buf[16];
	snprintf(buf, sizeof(buf), "%d", numbasefiles);
	fprintf(stderr,
		"BM_LogAndApply/%-6s   %8d iters : %9u us (%7.0f us / iter)\n",
		buf, iters, us, ((float)us) / iters);
}


int main(int argc, char* argv[]) {
	DBTest test;
	/*
	test.Empty();
	test.EmptyKey();
	test.EmptyValue();
	test.ReadWrite();
	test.PutDeleteGet();
	test.GetFromImmutableLayer();
	test.GetFromVersions();
	test.GetMemUsage();
	test.GetLevel0Ordering();
	test.GetOrderedByLevels();
	*/

	BMLogAndApply(1000, 1);
	BMLogAndApply(1000, 100);
	BMLogAndApply(1000, 10000);
	BMLogAndApply(100, 100000);
	return 0;
}


