#pragma once

#include "filename.h"
#include "dbimpl.h"
#include "logging.h"
#include "posix.h"
#include "versionset.h"
#include "logwriter.h"

class RecoveryTest {
public:
    RecoveryTest(const Options &options, const std::string &dbname)
            : env(new PosixEnv()),
              db(new DBImpl(options, dbname)),
              dbname(dbname) {
        db->DestroyDB(dbname, options);
        Open();
    }

    ~RecoveryTest() {
        db->DestroyDB(dbname, options);
    }

    void close() {
        db.Reset()();
    }

    void Open(Options *options = nullptr) {
        close();
        assert(openWithStatus(options).ok());
        assert(1 == numLogs());
    }

    Status openWithStatus(Options *options = nullptr) {
        close();
        Options opts;
        if (options != nullptr) {
            opts = *options;
        } else {
            opts.reuseLogs = true;  // TODO(sanjay): test both ways
            opts.createIfMissing = true;
        }

        if (opts.env == nullptr) {
            opts.env = env;
        }

        db = std::shared_ptr<DBImpl>(new DBImpl(opts, dbname));
        return db->Open();
    }

    bool canAppend() {
        std::shared_ptr <WritableFile> tmp;
        Status s = env->NewAppendableFile(CurrentFileName(dbname), tmp);
        if (s.IsNotSupportedError()) {
            return false;
        } else {
            return true;
        }
    }

    Status Put(const std::string &k, const std::string &v) {
        return db->Put(WriteOptions(), k, v);
    }

    std::string get(const std::string &k) {
        std::string result;
        Status s = db->get(ReadOptions(), k, &result);
        if (s.IsNotFound()) {
            result = "NOT_FOUND";
        } else if (!s.ok()) {
            result = s.toString();
        }
        return result;
    }

    std::string manifestFileName() {
        std::string current;
        assert(ReadFileToString(env.get(), CurrentFileName(dbname), &current).ok());
        size_t len = current.size();
        if (len > 0 && current[len - 1] == '\n') {
            current.resize(len - 1);
        }
        return dbname + "/" + current;
    }

    std::string logName(uint64_t number) {
        return LogFileName(dbname, number);
    }

    std::vector <uint64_t> getFiles(FileType t) {
        std::vector <std::string> filenames;
        assert(env->GetChildren(dbname, &filenames).ok());
        std::vector <uint64_t> result;
        for (size_t i = 0; i < filenames.size(); i++) {
            uint64_t number;
            FileType type;
            if (ParseFileName(filenames[i], &number, &type) && type == t) {
                result.push_back(number);
            }
        }
        return result;
    }

    size_t deleteLogFiles() {
        std::vector <uint64_t> logs = getFiles(kLogFile);
        for (size_t i = 0; i < logs.size(); i++) {
            assert(env->DeleteFile(logName(logs[i])).ok());
        }
        return logs.size();
    }

    void deleteManifestFile() {
        assert(env->DeleteFile(manifestFileName()).ok());
    }

    uint64_t firstLogFile() {
        return getFiles(kLogFile)[0];
    }

    int numLogs() {
        return getFiles(kLogFile).size();
    }

    int numTables() {
        return getFiles(kTableFile).size();
    }

    uint64_t filesize(const std::string &fname) {
        uint64_t result;
        assert(env->GetFileSize(fname, &result).ok());
        return result;
    }

    void manifestReused() {
        if (!canAppend()) {
            fprintf(stderr, "skipping test because env does not support appending\n");
            return;
        }

        assert(Put("foo", "bar").ok());
        close();

        std::string oldManifest = manifestFileName();
        Open();
        assert(oldManifest == manifestFileName());
        assert("bar" == get("foo"));
        Open();

        assert(oldManifest == manifestFileName());
        assert("bar" == get("foo"));
    }

    void largeManifestCompacted() {
        if (!canAppend()) {
            fprintf(stderr, "skipping test because env does not support appending\n");
            return;
        }

        Open();
        assert(Put("foo", "bar").ok());

        std::string oldManifest = manifestFileName();

        // Pad with zeroes to make manifest file very big.
        {
            uint64_t len = filesize(oldManifest);
            std::shared_ptr <WritableFile> file;
            options.env->NewAppendableFile(oldManifest, file);
            std::string zeroes(3 * 1048576 - static_cast<size_t>(len), 0);
            assert(file->append(zeroes).ok());
            assert(file->flush().ok());
        }

        Open();
        std::string newManifest = manifestFileName();
        assert(oldManifest != newManifest);
        assert(10000 > filesize(newManifest));
        assert("bar" == get("foo"));

        Open();
        assert(newManifest == manifestFileName());
        assert("bar" == get("foo"));
    }

    void noLogFiles() {
        assert(Put("foo", "bar").ok());
        assert(1 == deleteLogFiles());
        Open();
        assert("NOT_FOUND" == get("foo"));
        Open();
        assert("NOT_FOUND" == get("foo"));
    }

    void logFileReuse() {
        if (!canAppend()) {
            fprintf(stderr, "skipping test because env does not support appending\n");
            return;
        }

        for (int i = 0; i < 2; i++) {
            assert(Put("foo", "bar").ok());
            if (i == 0) {
                // Compact to ensure current log is Empty
                CompactMemTable();
            }

            close();
            assert(1 == numLogs());
            uint64_t number = firstLogFile();
            if (i == 0) {
                assert(0 == filesize(logName(number)));
            } else {
                assert(0 < filesize(logName(number)));
            }

            Open();
            assert(1 == numLogs());
            assert(number == firstLogFile());
            assert("bar" == get("foo"));
            Open();
            assert(1 == numLogs());
            assert(number == firstLogFile());
            assert("bar" == get("foo"));
        }
    }

    void multipleMemTables() {
        const int kNum = 1000;
        for (int i = 0; i < kNum; i++) {
            char buf[100];
            snprintf(buf, sizeof(buf), "%050d", i);
            assert(Put(buf, buf).ok());
        }

        assert(0 == numTables());
        close();
        assert(0 == numTables());
        assert(1 == numLogs());
        uint64_t oldLogFile = firstLogFile();

        // Force creation of multiple memtables by reducing the Write buffer size.
        Options opt;
        opt.reuseLogs = true;
        opt.writeBufferSize = (kNum * 100) / 2;
        Open(&opt);
        assert(2 <= numTables());
        assert(1 == numLogs());
        assert(oldLogFile != firstLogFile());
        for (int i = 0; i < kNum; i++) {
            char buf[100];
            snprintf(buf, sizeof(buf), "%050d", i);
            assert(buf == get(buf));
        }
    }

    void multipleLogFiles() {
        assert(Put("foo", "bar").ok());
        close();
        assert(1 == numLogs());

        // Make a bunch of uncompacted log files.
        uint64_t oldLog = firstLogFile();
        makeLogFile(oldLog + 1, 1000, "hello", "world");
        makeLogFile(oldLog + 2, 1001, "hi", "there");
        makeLogFile(oldLog + 3, 1002, "foo", "bar2");

        // Recover and check that all log files were processed.
        Open();
        assert(1 <= numTables());
        assert(1 == numLogs());
        uint64_t newLog = firstLogFile();
        assert(oldLog + 3 <= newLog);
        assert("bar2" == get("foo"));
        assert("world" == get("hello"));

        std::string str = get("hi");
        assert("there" == get("hi"));

        // Test that previous recovery produced recoverable state.
        Open();
        assert(1 <= numTables());
        assert(1 == numLogs());
        if (canAppend()) {
            assert(newLog == firstLogFile());
        }

        assert("bar2" == get("foo"));
        assert("world" == get("hello"));
        assert("there" == get("hi"));

        // Check that introducing an older log file does not cause it to be re-read.
        close();
        makeLogFile(oldLog + 1, 2000, "hello", "stale Write");
        Open();
        assert(1 <= numTables());
        assert(1 == numLogs());

        if (canAppend()) {
            assert(newLog == firstLogFile());
        }

        assert("bar2" == get("foo"));
        assert("world" == get("hello"));
        assert("there" == get("hi"));
    }

    void manifestMissing() {
        assert(Put("foo", "bar").ok());
        close();
        deleteManifestFile();

        Status status = openWithStatus();
        assert(status.IsCorruption());
    }

    void singleTables() {
        const int kNum = 1000000;
        for (int i = 0; i < kNum; i++) {
            char buf[100];
            snprintf(buf, sizeof(buf), "%050d", 100);
            assert(Put(buf, buf).ok());
        }

        close();
        Open();

        for (int i = 0; i < kNum; i++) {
            char buf[100];
            snprintf(buf, sizeof(buf), "%050d", 100);
            assert(buf == get(buf));
        }
    }

    void CompactMemTable() {
        db->TESTCompactMemTable();
    }

    // Directly construct a log file that sets key to val.
    void makeLogFile(uint64_t lognum, uint64_t seq, std::string_view key, std::string_view val) {
        std::string fname = LogFileName(dbname, lognum);
        std::shared_ptr <WritableFile> file;
        assert(env->NewWritableFile(fname, file).ok());
        LogWriter writer(file.get());
        WriteBatch batch;
        batch.Put(key, val);
        WriteBatchInternal::SetSequence(&batch, seq);
        assert(writer.AddRecord(WriteBatchInternal::Contents(&batch)).ok());
        assert(file->flush().ok());
    }

private:
    const Options options;
    const std::string dbname;
    std::shared_ptr <PosixEnv> env;
    std::shared_ptr <DBImpl> db;
};

int main() {
    Options options;
    const std::string dbname = "recovery";
    RecoveryTest rtest(options, dbname);
    /*rtest.manifestReused();
    rtest.largeManifestCompacted();
    rtest.noLogFiles();
    rtest.logFileReuse();
    rtest.multipleMemTables();
    */
    rtest.multipleLogFiles();
    return 0;
}
