#include "redisdb.h"

const int KEYLENGTH = 16;
const int VALUELENGTH = 100;
const int THREADNUM = 1;
const int HASH_TABLE_FIELD_SIZE = 10000000;

using namespace std::chrono;

static const std::string key(KEYLENGTH, 'a');
static const std::string value(VALUELENGTH, 'a');

void BenchSet()
{
	printf("====== Set ======\n");
	Options opts;
	opts.createifmissing = true;
	opts.writebuffersize = 4 * 1024 * 1024;
	opts.blocksize = 4 * 1024;

	RedisDB db(opts, "./dbbench");
	Status s = db.Open();
	if (s.ok()) {
		printf("Open success\n");
	}
	else {
		printf("Open failed, error: %s\n", s.ToString().c_str());
		return;
	}

	std::vector<std::thread> jobs;
	size_t kv_num = 1000000;
	jobs.clear();

	auto start = std::chrono::system_clock::now();
	for (size_t i = 0; i < THREADNUM; ++i) {
		jobs.emplace_back([&db](size_t kv_num) {
			for (size_t j = 0; j < kv_num; ++j)	{
				db.Set(key, value);
			}
		}, kv_num);
	}

	for (auto &job : jobs) {
		job.join();
	}

	auto end = system_clock::now();
	duration<double> elapsed_seconds = end - start;
	auto cost = duration_cast<std::chrono::seconds>(elapsed_seconds).count();
	std::cout << "Test case 1, Set " << THREADNUM * kv_num << " Cost: "
			  << cost << "s QPS: " << (THREADNUM * kv_num) / cost << std::endl;

	kv_num = 1000000;
	jobs.clear();
	start = system_clock::now();
	for (size_t i = 0; i < THREADNUM; ++i) {
		jobs.emplace_back([&db](size_t kv_num) {
			for (size_t j = 0; j < kv_num; ++j) {
				db.Set(key, value);
			}
		}, kv_num);
	}

	for (auto &job : jobs) {
		job.join();
	}

	end = system_clock::now();
	elapsed_seconds = end - start;
	cost = duration_cast<seconds>(elapsed_seconds).count();
	std::cout << "Test case 2, Set " << THREADNUM * kv_num << " Cost: "
			  << cost << "s QPS: " << (THREADNUM * kv_num) / cost << std::endl;
}

int main()
{
	BenchSet();
	return 0;
}
