#pragma once

#include <map>
#include <string>
#include <assert.h>
#include <string_view>
#include <iostream>
#include <random>
#include "table.h"
#include "dbformat.h"
#include "memtable.h"
#include "dbimpl.h"
#include "posix.h"
#include "blockbuilder.h"
#include "tablebuilder.h"


// Return reverse of "key".
// Used to test non-lexicographic comparators.
std::string reverse(const std::string_view &key) {
    std::string str(key.data(), key.size());
    std::string rev("");
    for (std::string::reverse_iterator rit = str.rbegin();
         rit != str.rend(); ++rit) {
        rev.push_back(*rit);
    }
    return rev;
}

class ReverseKeyComparator : public Comparator {
public:
    ReverseKeyComparator()
            : comparator(new BytewiseComparatorImpl()) {

    }

    virtual const char *name() const {
        return "leveldb.ReverseBytewiseComparator";
    }

    virtual int compare(const std::string_view &a, const std::string_view &b) const {
        return comparator->compare(reverse(a), reverse(b));
    }

    virtual void FindShortestSeparator(
            std::string *start,
            const std::string_view &limit) const {
        std::string s = reverse(*start);
        std::string l = reverse(limit);
        comparator->FindShortestSeparator(&s, l);
        *start = reverse(s);
    }

    virtual void FindShortSuccessor(std::string *key) const {
        std::string s = reverse(*key);
        comparator->FindShortSuccessor(&s);
        *key = reverse(s);
    }

private:
    std::shared_ptr <Comparator> comparator;
};

std::shared_ptr <ReverseKeyComparator> reverseCmp = nullptr;

static void increment(const Comparator *cmp, std::string *key) {
    if (!reverseCmp) {
        key->push_back('\0');
    } else {
        std::string rev = reverse(*key);
        rev.push_back('\0');
        *key = reverse(rev);
    }
}

// Helper class for tests to unify the interface between
// BlockBuilder/TableBuilder and Block/Table.

struct STLLessThan {
    const Comparator *cmp;

    STLLessThan(const Comparator *c)
            : cmp(c) {

    }

    bool operator()(const std::string &a, const std::string &b) const {
        return cmp->compare(std::string_view(a), std::string_view(b)) < 0;
    }
};

class StringSink : public WritableFile {
public:
    virtual ~StringSink() {}

    const std::string &getContents() const { return Contents; }

    virtual Status close() { return Status::OK(); }

    virtual Status flush() { return Status::OK(); }

    virtual Status sync() { return Status::OK(); }

    virtual Status append(const std::string_view &data) {
        Contents.append(data.data(), data.size());
        return Status::OK();
    }

private:
    std::string Contents;
};

class StringSource : public RandomAccessFile {
public:
    StringSource(const std::string_view &c)
            : Contents(c.data(), c.size()) {

    }

    virtual ~StringSource() {}

    uint64_t size() const { return Contents.size(); }

    virtual Status read(uint64_t offset, size_t n, std::string_view *result,
                        char *scratch) const {
        if (offset > Contents.size()) {
            return Status::InvalidArgument("invalid Read offset");
        }

        if (offset + n > Contents.size()) {
            n = Contents.size() - offset;
        }

        memcpy(scratch, &Contents[offset], n);
        *result = std::string_view(scratch, n);
        return Status::OK();
    }

private:
    std::string Contents;
};

typedef std::map <std::string, std::string, STLLessThan> KVMap;

class Constructor {
public:
    explicit Constructor(const Comparator *cmp)
            : data(STLLessThan(cmp)) {}

    virtual ~Constructor() {}

    void Add(const std::string &key, const std::string_view &value) {
        data[key] = std::string(value.data(), value.size());
    }

    // Finish constructing the data structure with all the keys that have
    // been added so far.  Returns the keys in sorted order in "*keys"
    // and stores the key/value pairs in "*kvmap"
    void Finish(const Options &options,
                std::vector <std::string> *keys,
                KVMap *kvmap) {
        kvmap->clear();
        *kvmap = data;
        keys->clear();
        for (auto &it : data) {
            keys->push_back(it.first);
        }

        data.clear();
        Status s = finishImpl(options, *kvmap);
        assert(s.ok());
    }

    // Construct the data structure from the data in "data"
    virtual Status finishImpl(const Options &options, const KVMap &data, bool store = true) = 0;

    virtual const KVMap &GetData() { return data; }

    virtual std::shared_ptr <Iterator> NewIterator() const = 0;

private:
    KVMap data;
};

class BlockConstructor : public Constructor {
public:
    explicit BlockConstructor(const Comparator *cmp)
            : Constructor(cmp),
              comparator(cmp),
              block(nullptr) {

    }

    ~BlockConstructor() {

    }

    virtual std::shared_ptr <Iterator> NewIterator() const {
        return block->NewIterator(comparator);
    }

    virtual Status finishImpl(const Options &options, const KVMap &data, bool store = true) {
        BlockBuilder builder(&options);

        for (auto &it : data) {
            builder.Add(it.first, it.second);
        }

        content = builder.Finish();
        BlockContents Contents;
        Contents.data = content;
        Contents.cachable = false;
        Contents.heapAllocated = false;
        block.Reset()(new Block(Contents));
        return Status::OK();
    }

private:
    const Comparator *comparator;
    std::string content;
    std::shared_ptr <Block> block;

    BlockConstructor();
};

class TableConstructor : public Constructor {
public:
    TableConstructor(const Comparator *cmp)
            : Constructor(cmp),
              filename("TableConstructor"),
              source(nullptr) {

    }

    ~TableConstructor() {

    }

    virtual Status finishImpl(const Options &options, const KVMap &data, bool store = true) {
        table.Reset()();
        source.Reset()();

        Status s;
        if (store) {
            std::shared_ptr <StringSink> sink(new StringSink());
            TableBuilder builder(options, sink);
            for (auto it = data.begin(); it != data.end(); ++it) {
                builder.Add(it->first, it->second);
                assert(builder.status().ok());
            }

            s = builder.Finish();
            assert(s.ok());
            assert(sink->getContents().size() == builder.filesize());
            source = std::shared_ptr<StringSource>(new StringSource(sink->getContents()));
            Options tableOptions;
            tableOptions.comparator = options.comparator;
            return Table::Open(tableOptions, source, sink->getContents().size(), table);
        } else {
            std::shared_ptr <WritableFile> wfile;
            s = options.env->NewWritableFile(filename, wfile);
            assert(s.ok());


            TableBuilder builder(options, wfile);
            for (auto it = data.begin(); it != data.end(); ++it) {
                builder.Add(it->first, it->second);
                assert(builder.status().ok());
            }

            s = builder.Finish();
            assert(s.ok());
            s = wfile->sync();
            assert(s.ok());
            s = wfile->close();
            assert(s.ok());

            uint64_t size;
            s = options.env->GetFileSize(filename, &size);
            assert(s.ok());
            assert(size == builder.filesize());

            std::shared_ptr <RandomAccessFile> mfile = nullptr;
            s = options.env->NewRandomAccessFile(filename, mfile);
            assert(s.ok());
            return Table::Open(options, mfile, builder.filesize(), table);
        }
    }

    virtual std::shared_ptr <Iterator> NewIterator() const {
        return table->NewIterator(ReadOptions());
    }

private:
    std::shared_ptr <Table> table;
    std::shared_ptr <StringSource> source;
    std::string filename;
};

class KeyConvertingIterator : public Iterator {
public:
    explicit KeyConvertingIterator(const std::shared_ptr <Iterator> &iter)
            : iter(iter) {


    }

    virtual ~KeyConvertingIterator() {

    }

    virtual bool Valid() const { return iter->Valid(); }

    virtual void Seek(const std::string_view &target) {
        ParsedInternalKey ikey(target, kMaxSequenceNumber, kTypeValue);
        std::string encoded;
        AppendInternalKey(&encoded, ikey);
        iter->Seek(encoded);
    }

    virtual void SeekToFirst() { iter->SeekToFirst(); }

    virtual void SeekToLast() { iter->SeekToLast(); }

    virtual void Next() { iter->Next(); }

    virtual void Prev() { iter->Prev(); }

    virtual std::string_view key() const {
        assert(Valid());
        ParsedInternalKey key;
        if (!ParseInternalKey(iter->key(), &key)) {
            s = Status::Corruption("malformed internal key");
            return std::string_view("corrupted key");
        }
        return key.userKey;
    }

    virtual std::string_view value() const { return iter->value(); }

    virtual Status status() const {
        return s.ok() ? iter->status() : s;
    }

    virtual void RegisterCleanup(const std::any &arg) {}

private:
    mutable Status s;
    std::shared_ptr <Iterator> iter;

    // No copying allowed
    KeyConvertingIterator(const KeyConvertingIterator &);

    void operator=(const KeyConvertingIterator &);
};

class MemTableConstructor : public Constructor {
public:
    explicit MemTableConstructor(const Comparator *cmp)
            : Constructor(cmp),
              icmp(cmp),
              memtable(new MemTable(icmp)) {

    }

    virtual Status finishImpl(const Options &options, const KVMap &data, bool store = true) {
        memtable.Reset()(new MemTable(icmp));
        int seq = 1;
        for (auto &it : data) {
            memtable->Add(seq, kTypeValue, it.first, it.second);
            seq++;
        }
        return Status::OK();
    }

    virtual std::shared_ptr <Iterator> NewIterator() const {
        std::shared_ptr <Iterator> iter(new KeyConvertingIterator(memtable->NewIterator()));
        return iter;
    }

private:
    InternalKeyComparator icmp;
    std::shared_ptr <MemTable> memtable;
};

enum TestType {
    TABLE_TEST,
    BLOCK_TEST,
    MEMTABLE_TEST,
    DB_TEST
};

struct TestArgs {
    TestType type;
    bool reverseCompare;
    int restartInterval;
};

static const TestArgs kTestArgList[] =
        {
                {TABLE_TEST,    false, 16},
                {TABLE_TEST,    false, 1},
                {TABLE_TEST,    false, 1024},
                {TABLE_TEST,    true,  16},
                {TABLE_TEST,    true,  1},
                {TABLE_TEST,    true,  1024},
                {BLOCK_TEST,    false, 16},
                {BLOCK_TEST,    false, 1},
                {BLOCK_TEST,    false, 1024},
                {BLOCK_TEST,    true,  16},
                {BLOCK_TEST,    true,  1},
                {BLOCK_TEST,    true,  1024},

                // Restart interval does not matter for memtables
                {MEMTABLE_TEST, false, 16},
                {MEMTABLE_TEST, true,  16},
        };

static const int kNumTestArgs = sizeof(kTestArgList) / sizeof(kTestArgList[0]);

class Harness {
public:
    Harness()
            : constructor(nullptr) {

    }

    void init(const TestArgs &args) {
        constructor.Reset()();
        options = Options();
        options.blockRestartInterval = args.restartInterval;
        options.blockSize = 256;

        if (args.reverseCompare) {
            reverseCmp = std::shared_ptr<ReverseKeyComparator>(new ReverseKeyComparator());
            options.comparator = reverseCmp.get();
        } else {
            reverseCmp.Reset()();
        }

        switch (args.type) {
            case TABLE_TEST:
                constructor = std::shared_ptr<TableConstructor>(new TableConstructor(options.comparator));
                break;
            case BLOCK_TEST:
                constructor = std::shared_ptr<BlockConstructor>(new BlockConstructor(options.comparator));
                break;
            case MEMTABLE_TEST:
                constructor = std::shared_ptr<MemTableConstructor>(new MemTableConstructor(options.comparator));
                break;
        }
    }

    ~Harness() {

    }

    void Add(const std::string &key, const std::string &value) {
        constructor->Add(key, value);
    }

    std::string toString(const KVMap &data, const KVMap::const_iterator &it) {
        if (it == data.end()) {
            return "END";
        } else {
            return "'" + it->first + "->" + it->second + "'";
        }
    }

    std::string toString(const KVMap &data,
                         const KVMap::const_reverse_iterator &it) {
        if (it == data.rend()) {
            return "END";
        } else {
            return "'" + it->first + "->" + it->second + "'";
        }
    }

    std::string toString(const std::shared_ptr <Iterator> &it) {
        if (!it->Valid()) {
            return "END";
        } else {
            key = it->key();
            value = it->value();
            return "'" + key + "->" + value + "'";
        }
    }

    std::string pickRandomKey(const std::vector <std::string> &keys) {
        if (keys.Empty()) {
            return "foo";
        } else {
            std::default_random_engine engine;
            const int index = engine() % keys.size();
            std::string result = keys[index];
            switch (engine() % 3) {
                case 0: {
                    break;
                }
                case 1: {
                    // Attempt to return something smaller than an existing key
                    if (result.size() > 0 && result[result.size() - 1] > '\0') {
                        result[result.size() - 1];
                    }
                    break;
                }
                case 2: {
                    // Return something larger than an existing key
                    increment(options.comparator, &result);
                    break;
                }
            }
            return result;
        }
    }

    void test() {
        std::vector <std::string> keys;
        KVMap data(options.comparator);
        constructor->Finish(options, &keys, &data);
        testForwardScan(keys, data);
        testBackwardScan(keys, data);
        testRandomAccess(keys, data);
    }

    void testForwardScan(const std::vector <std::string> &keys,
                         const KVMap &data) {
        std::shared_ptr <Iterator> iter = constructor->NewIterator();
        assert(!iter->Valid());
        iter->SeekToFirst();

        for (KVMap::const_iterator it = data.begin();
             it != data.end(); ++it) {
            std::string k = toString(data, it);
            std::string k1 = toString(iter);

            assert(k == k1);
            iter->Next();
        }
        assert(!iter->Valid());
    }

    void testBackwardScan(const std::vector <std::string> &keys,
                          const KVMap &data) {
        std::shared_ptr <Iterator> iter = constructor->NewIterator();
        assert(!iter->Valid());
        iter->SeekToLast();

        for (KVMap::const_reverse_iterator it = data.rbegin();
             it != data.rend(); ++it) {
            std::string k = toString(data, it);
            std::string k1 = toString(iter);

            assert(k == k1);
            iter->Prev();
        }
        assert(!iter->Valid());
    }

    void testRandomAccess(const std::vector <std::string> &keys,
                          const KVMap &data) {
        static const bool kVerbose = false;
        std::shared_ptr <Iterator> iter = constructor->NewIterator();
        assert(!iter->Valid());
        KVMap::const_iterator it = data.begin();
        if (kVerbose) fprintf(stderr, "---\n");

        for (int i = 0; i < 200; i++) {
            std::default_random_engine engine;
            const int toss = engine() % 5;
            switch (toss) {
                case 0: {
                    if (iter->Valid()) {
                        if (kVerbose) fprintf(stderr, "Next\n");
                        iter->Next();
                        ++it;
                        std::string k = toString(data, it);
                        std::string k1 = toString(iter);
                        assert(k == k1);
                    }
                    break;
                }
                case 1: {
                    if (kVerbose) fprintf(stderr, "SeekToFirst\n");
                    iter->SeekToFirst();
                    it = data.begin();

                    std::string k = toString(data, it);
                    std::string k1 = toString(iter);
                    assert(k == k1);
                    break;
                }
                case 2: {
                    std::string key = pickRandomKey(keys);
                    it = data.lower_bound(key);
                    if (kVerbose)
                        fprintf(stderr, "Seek '%s'\n",
                                key.c_str());
                    iter->Seek(std::string_view(key));
                    std::string k = toString(data, it);
                    std::string k1 = toString(iter);
                    assert(k == k1);
                    break;
                }
                case 3: {
                    if (iter->Valid()) {
                        if (kVerbose) fprintf(stderr, "Prev\n");
                        iter->Prev();
                        if (it == data.begin()) {
                            it = data.end();
                        } else {
                            --it;
                        }

                        std::string k = toString(data, it);
                        std::string k1 = toString(iter);
                        assert(k == k1);
                    }
                    break;
                }
                case 4: {
                    if (kVerbose) fprintf(stderr, "SeekToLast\n");
                    iter->SeekToLast();
                    if (keys.Empty()) {
                        it = data.end();
                    } else {
                        std::string last = data.rbegin()->first;
                        it = data.lower_bound(last);
                        std::string k = toString(data, it);
                        std::string k1 = toString(iter);
                        assert(k == k1);
                    }
                    break;
                }
            }
        }
    }

private:
    Options options;
    std::shared_ptr <Constructor> constructor;
    std::string key;
    std::string value;
};

/*
int main()
{	
	// Test the Empty key
	for (int i = 0; i < kNumTestArgs; i++) 
	{
		Harness arness ;
		arness.init(kTestArgList[i]);
		arness.Add("k", "v");
		arness.Add("k", "v");
		arness.Add("k", "v0");
		arness.Add("k1", "v1");
		arness.Add("k2", "v2");
		arness.test();
	}
	return 0;
}
*/
