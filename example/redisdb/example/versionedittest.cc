#pragma once

#include "versionedit.h"

class VersionEditTest {
public:
    void testEncodeDecode(const VersionEdit &edit) {
        std::string encoded, encoded2;
        edit.EncodeTo(&encoded);
        VersionEdit parsed;
        Status s = parsed.DecodeFrom(encoded);
        assert(s.ok());
        parsed.EncodeTo(&encoded2);
        assert(encoded == encoded2);
    }

    void encodeDecode() {
        static const uint64_t kBig = 1ull << 50;
        VersionEdit edit;

        for (int i = 0; i < 4; i++) {
            testEncodeDecode(edit);
            edit.AddFile(3, kBig + 300 + i, kBig + 400 + i,
                         InternalKey("foo", kBig + 500 + i, kTypeValue),
                         InternalKey("zoo", kBig + 600 + i, kTypeDeletion));
            edit.DeleteFile(4, kBig + 700 + i);
            edit.SetCompactPointer(i, InternalKey("x", kBig + 900 + i, kTypeValue));
        }

        edit.SetComparatorName("foo");
        edit.SetLogNumber(kBig + 100);
        edit.SetNextFile(kBig + 200);
        edit.SetLastSequence(kBig + 1000);
        testEncodeDecode(edit);
    }
};

/*
int main()
{
	VersionEditTest vtest;
	vtest.encodeDecode();
	return 0;
}
*/
