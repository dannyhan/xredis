#include "redisdb.h"

int main() {
	Options opts;
	opts.createifmissing = true;
	RedisDB db(opts, "./db");
	Status s = db.Open();
	if (s.ok()) {
		printf("Open success\n");
	}
	else {
		printf("Open failed, error: %s\n", s.ToString().c_str());
		return -1;
	}

	int32_t ret;
	// Set
	s = db.Set("TEST_KEY", "TEST_VALUE");
	printf("Set return: %s\n", s.ToString().c_str());

	// Get
	std::string value;
	s = db.Get("TEST_KEY", &value);
	printf("Get return: %s, value: %s\n", s.ToString().c_str(), value.c_str());

	// SetBit
	s = db.SetBit("SETBIT_KEY", 7, 1, &ret);
	printf("SetBit return: %s, ret: %d\n",
		s.ToString().c_str(), ret);

	// GetSet
	s = db.GetSet("TEST_KEY", "Hello", &value);
	printf("GetSet return: %s, old_value: %s",
		s.ToString().c_str(), value.c_str());

	// SetBit
	s = db.SetBit("SETBIT_KEY", 7, 1, &ret);
	printf("Setbit return: %s\n", s.ToString().c_str());

	// GetBit
	s = db.GetBit("SETBIT_KEY", 7, &ret);
	printf("GetBit return: %s, ret: %d\n",
		s.ToString().c_str(), ret);

	// MSet
	std::vector<KeyValue> kvs;
	kvs.push_back({"TEST_KEY1", "TEST_VALUE1"});
	kvs.push_back({"TEST_KEY2", "TEST_VALUE2"});
	s = db.MSet(kvs);
	printf("MSet return: %s\n", s.ToString().c_str());

	// MGet
	std::vector<ValueStatus> values;
	std::vector<std::string> keys {"TEST_KEY1",
		"TEST_KEY2", "TEST_KEY_NOT_EXIST"};
	s = db.MGet(keys, &values);
	printf("MGet return: %s\n", s.ToString().c_str());
	for (size_t idx = 0; idx != keys.size(); idx++) {
	printf("idx = %d, keys = %s, value = %s\n",
		idx, keys[idx].c_str(), values[idx].value.c_str());
	}

	// Setnx
	s = db.Setnx("TEST_KEY", "TEST_VALUE", &ret);
	printf("Setnx return: %s, value: %s, ret: %d\n",
		s.ToString().c_str(), value.c_str(), ret);

	// MSetnx
	s = db.MSetnx(kvs, &ret);
	printf("MSetnx return: %s, ret: %d\n", s.ToString().c_str(), ret);

	// Setrange
	s = db.Setrange("TEST_KEY", 10, "APPEND_VALUE", &ret);
	printf("Setrange return: %s, ret: %d\n", s.ToString().c_str(), ret);

	// Getrange
	s = db.Getrange("TEST_KEY", 0, -1, &value);
	printf("Getrange return: %s, value: %s\n",
			s.ToString().c_str(), value.c_str());

	// Append
	std::string appendvalue;
	s = db.Set("TEST_KEY", "TEST_VALUE");
	s = db.Append("TEST_KEY", "APPEND_VALUE", &ret);
	s = db.Get("TEST_KEY", &appendvalue);
	printf("Append return: %s, value: %s, ret: %d\n",
		s.ToString().c_str(), appendvalue.c_str(), ret);

	// BitCount
	s = db.BitCount("TEST_KEY", 0, -1, &ret, false);
	printf("BitCount return: %s, ret: %d\n", s.ToString().c_str(), ret);

	// BitCount
	s = db.BitCount("TEST_KEY", 0, -1, &ret, true);
	printf("BitCount return: %s, ret: %d\n", s.ToString().c_str(), ret);

	// BitOp
	/*
	int64_t bitopret;
	s = db.Set("BITOP_KEY1", "FOOBAR");
	s = db.Set("BITOP_KEY2", "ABCDEF");
	s = db.Set("BITOP_KEY3", "REDISDB");
	std::vector<std::string> srckeys {"BITOP_KEY1", "BITOP_KEY2", "BITOP_KEY3"};
	// and
	s = db.BitOp(BitOpType::kBitOpAnd,
				"BITOP_DESTKEY", srckeys, &bitopret);
	printf("BitOp return: %s, ret: %d\n", s.ToString().c_str(), bitopret);
	// or
	s = db.BitOp(BitOpType::kBitOpOr,
				"BITOP_DESTKEY", srckeys, &bitopret);
	printf("BitOp return: %s, ret: %d\n", s.ToString().c_str(), bitopret);
	// xor
	s = db.BitOp(BitOpType::kBitOpXor,
				"BITOP_DESTKEY", srckeys, &bitopret);
	printf("BitOp return: %s, ret: %d\n", s.ToString().c_str(), bitopret);
	// not
	std::vector<std::string> notkeys {"BITOP_KEY1"};
	s = db.BitOp(BitOpType::kBitOpNot,
				"BITOP_DESTKEY", notkeys, &bitopret);
	printf("BitOp return: %s, ret: %d\n", s.ToString().c_str(), bitopret);
	

	// BitPos
	
	int64_t bitposret;
	s = db.Set("BITPOS_KEY", "\xff\x00\x00");
	// bitpos key bit
	s = db.BitPos("BITPOS_KEY", 1, &bitposret);
	printf("BitPos return: %s, ret: %d\n", s.ToString().c_str(), bitposret);
	// bitpos key bit [start]
	s = db.BitPos("BITPOS_KEY", 1, 0, &bitposret);
	printf("BitPos return: %s, ret: %d\n", s.ToString().c_str(), bitposret);
	// bitpos key bit [start] [end]
	s = db.BitPos("BITPOS_KEY", 1, 0, 4, &bitposret);
	printf("BitPos return: %s, ret: %d\n", s.ToString().c_str(), bitposret);

	// Decrby
	int64_t decrbyret;
	s = db.Set("TEST_KEY", "12345");
	s = db.Decrby("TEST_KEY", 5, &decrbyret);
	printf("Decrby return: %s, ret: %d\n", s.ToString().c_str(), decrbyret);

	// Incrby
	int64_t incrbyret;
	s = db.Incrby("INCRBY_KEY", 5, &incrbyret);
	printf("Incrby return: %s, ret: %d\n", s.ToString().c_str(), incrbyret);

	// Incrbyfloat
	s = db.Set("INCRBYFLOAT_KEY", "10.50");
	s = db.Incrbyfloat("INCRBYFLOAT_KEY", "0.1", &value);
	printf("Incrbyfloat return: %s, value: %s\n",
			s.ToString().c_str(), value.c_str());

	// Setex
	s = db.Setex("TEST_KEY", "TEST_VALUE", 1);
	printf("Setex return: %s\n", s.ToString().c_str());
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	s = db.Get("TEST_KEY", &value);
	printf("Get return: %s, value: %s\n", s.ToString().c_str(), value.c_str());
	*/

	// Strlen
	s = db.Set("TEST_KEY", "TEST_VALUE");
	int32_t len = 0;
	s = db.Strlen("TEST_KEY", &len);
	printf("Strlen return: %s, strlen: %d\n", s.ToString().c_str(), len);


	// Expire
	std::map<DataType, Status> keystatus;
	s = db.Set("EXPIRE_KEY", "EXPIREVALUE");
	printf("Set return: %s\n", s.ToString().c_str());
	db.Expire("EXPIRE_KEY", 1, &keystatus);
	std::this_thread::sleep_for(std::chrono::milliseconds(2500));
	s = db.Get("EXPIRE_KEY", &value);
	printf("Get return: %s, value: %s\n", s.ToString().c_str(), value.c_str());

	// Compact
	//s = db.Compact();
	//printf("Compact return: %s\n", s.ToString().c_str());


	// SAdd
	std::vector<std::string> members {"MM1", "MM2", "MM3", "MM2"};
	s = db.SAdd("SADD_KEY", members, &ret);
	printf("SAdd return: %s, ret = %d\n", s.ToString().c_str(), ret);

	// SCard
	ret = 0;
	s = db.SCard("SADD_KEY", &ret);
	printf("SCard, return: %s, scard ret = %d\n", s.ToString().c_str(), ret);


	// HSet
	int32_t res;
	s = db.HSet("TEST_KEY1", "TEST_FIELD1", "TEST_VALUE1", &res);
	printf("HSet return: %s, res = %d\n", s.ToString().c_str(), res);
	s = db.HSet("TEST_KEY1", "TEST_FIELD2", "TEST_VALUE2", &res);
	printf("HSet return: %s, res = %d\n", s.ToString().c_str(), res);

	s = db.HSet("TEST_KEY2", "TEST_FIELD1", "TEST_VALUE1", &res);
	printf("HSet return: %s, res = %d\n", s.ToString().c_str(), res);
	s = db.HSet("TEST_KEY2", "TEST_FIELD2", "TEST_VALUE2", &res);
	printf("HSet return: %s, res = %d\n", s.ToString().c_str(), res);
	s = db.HSet("TEST_KEY2", "TEST_FIELD3", "TEST_VALUE3", &res);
	printf("HSet return: %s, res = %d\n", s.ToString().c_str(), res);

	// HGet
	std::string value;
	s = db.HGet("TEST_KEY1", "TEST_FIELD1", &value);
	printf("HGet return: %s, value = %s\n", s.ToString().c_str(), value.c_str());
	s = db.HGet("TEST_KEY1", "TEST_FIELD2", &value);
	printf("HGet return: %s, value = %s\n", s.ToString().c_str(), value.c_str());
	s = db.HGet("TEST_KEY1", "TEST_FIELD3", &value);
	printf("HGet return: %s, value = %s\n", s.ToString().c_str(), value.c_str());
	s = db.HGet("TEST_KEY_NOT_EXIST", "TEST_FIELD", &value);
	printf("HGet return: %s, value = %s\n", s.ToString().c_str(), value.c_str());

	return 0;
}

