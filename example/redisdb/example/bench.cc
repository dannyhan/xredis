#include "redisdb.h"
#include "histogram.h"
#include "crc32c.h"

// Comma-separated list of operations to run in the specified order
//   Actual benchmarks:
//      fillseq       -- write N values in sequential key order in async mode
//      fillrandom    -- write N values in random key order in async mode
//      overwrite     -- overwrite N values in random key order in async mode
//      fillsync      -- write N/100 values in random key order in sync mode
//      fill100K      -- write N/1000 100K values in random order in async mode
//      deleteseq     -- delete N keys in sequential order
//      deleterandom  -- delete N keys in random order
//      readseq       -- read N times sequentially
//      readreverse   -- read N times in reverse order
//      readrandom    -- read N times in random order
//      readmissing   -- read N missing keys in random order
//      readhot       -- read N times in random order from 1% section of DB
//      seekrandom    -- N random seeks
//      open          -- cost of opening a DB
//      crc32c        -- repeated crc32c of 4K of data
//   Meta operations:
//      compact     -- Compact the entire DB
//      stats       -- Print DB stats
//      sstables    -- Print sstable info
//      heapprofile -- Dump a heap profile (if supported by this port)
static const char* FLAGS_benchmarks =
	"fillseq,"
	"fillsync,"
	"fillrandom,"
	"overwrite,"
	"readrandom,"
	"readrandom,"
	"readseq,"
	"readreverse,"
	"compact,"
	"readrandom,"
	"readseq,"
	"readreverse,"
	"fill100K,"
	"crc32c,"
	"snappycomp,"
	"snappyuncomp,";


// Number of key/values to place in database
static int FLAGS_num = 1000000;

// Number of read operations to do.  If negative, do FLAGS_num reads.
static int FLAGS_reads = -1;

// Number of concurrent threads to run.
static int FLAGS_threads = 1;

// Size of each value
static int FLAGS_value_size = 100;

// Arrange to generate values that shrink to this fraction of
// their original size after compression
static double FLAGS_compression_ratio = 0.5;

// Print histogram of operation timings
static bool FLAGS_histogram = false;

// Number of bytes to buffer in memtable before compacting
// (initialized to default value by "main")
static int FLAGS_write_buffer_size = 4 * 1024 * 1024;;

// Number of bytes written to each file.
// (initialized to default value by "main")
static int FLAGS_max_file_size = 2 * 1024 * 1024;

// Approximate size of user data packed per block (before compression.
// (initialized to default value by "main")
static int FLAGS_block_size = 4 * 1024;

// Number of bytes to use as a cache of uncompressed data.
// Negative means use default settings.
static int FLAGS_cache_size = -1;

// Maximum number of files to keep open at the same time (use default if == 0)
static int FLAGS_open_files = 0;

// Bloom filter bits per key.
// Negative means use default settings.
static int FLAGS_bloom_bits = -1;

// If true, do not destroy the existing database.  If you set this
// flag and also specify a benchmark that wants a fresh database, that
// benchmark will fail.
static bool FLAGS_use_existing_db = false;

// If true, reuse existing log/MANIFEST files when re-opening a database.
static bool FLAGS_reuse_logs = false;

// Use the db with the following name.
static const char* FLAGS_db = nullptr;

class RandomGenerator {
private:
	std::string data;
	int pos;

	public:
	RandomGenerator() {
		// We use a limited amount of data over and over again and ensure
		// that it is larger than the compression window (32KB), and also
		// large enough to serve all typical value sizes we want to write.
		std::default_random_engine rnd(301);
		std::string piece;
		while (data.size() < 1048576) {
			// Add a short fragment that is as compressible as specified
			// by FLAGS_compression_ratio.
			CompressibleString(&rnd, FLAGS_compression_ratio, 100, &piece);
			data.append(piece);
		}
		pos = 0;
	}

	std::string_view Generate(size_t len) {
		if (pos + len > data.size()) {
			pos = 0;
			assert(len < data.size());
		}

		pos += len;
		return std::string_view(data.data() + pos - len, len);
	}
};

#if defined(__linux)
static std::string_view TrimSpace(std::string_view s) {
	size_t start = 0;
	while (start < s.size() && isspace(s[start])) {
		start++;
	}
	
	size_t limit = s.size();
	while (limit > start && isspace(s[limit - 1])) {
		limit--;
	}
	return  std::string_view(s.data() + start, limit - start);
}
#endif

static void AppendWithSpace(std::string* str, std::string_view msg) {
	if (msg.empty()) return;
	if (!str->empty()) {
		str->push_back(' ');
	}
	str->append(msg.data(), msg.size());
}

class Stats {
private:
	double start;
	double finish;
	double seconds;
	int done;
	int nextreport;
	int64_t bytes;
	double lastopfinish;
	Histogram hist;
	std::string message;
	Env env;
public:
	Stats() { Start(); }

	void Start() {
		nextreport = 100;
		lastopfinish = start;
		hist.Clear();
		done = 0;
		bytes = 0;
		seconds = 0;
		start = env.NowMicros();
		finish = start;
		message.clear();
	}

	void Merge(const Stats& other) {
		hist.Merge(other.hist);
		done += other.done;
		bytes += other.bytes;
		seconds += other.seconds;
		if (other.start < start) start = other.start;
		if (other.finish > finish) finish = other.finish;

		// Just keep the messages from one thread
		if (message.empty()) message = other.message;
	}

	void Stop() {
		finish = env.NowMicros();
		seconds = (finish - start) * 1e-6;
	}

	void AddMessage(std::string_view msg) { AppendWithSpace(&message, msg); }

	void FinishedSingleOp() {
		if (FLAGS_histogram) {
			double now = env.NowMicros();
			double micros = now - lastopfinish;
			hist.Add(micros);
			if (micros > 20000) {
				fprintf(stderr, "long op: %.1f micros%30s\r", micros, "");
				fflush(stderr);
			}
			lastopfinish = now;
		}

		done++;
		if (done >= nextreport) {
			if (nextreport < 1000)
				nextreport += 100;
			else if (nextreport < 5000)
			nextreport += 500;
			else if (nextreport < 10000)
			nextreport += 1000;
			else if (nextreport < 50000)
			nextreport += 5000;
			else if (nextreport < 100000)
			nextreport += 10000;
			else if (nextreport < 500000)
			nextreport += 50000;
			else
			nextreport += 100000;
			fprintf(stderr, "... finished %d ops%30s\r", done, "");
			fflush(stderr);
		}
	}

	void AddBytes(int64_t n) { bytes += n; }

	void Report(const std::string_view& name) {
		// Pretend at least one op was done in case we are running a benchmark
		// that does not call FinishedSingleOp().
		if (done < 1) done = 1;

		std::string extra;
		if (bytes > 0) {
			// Rate is computed on actual elapsed time, not the sum of per-thread
			// elapsed times.
			double elapsed = (finish - start) * 1e-6;
			char rate[100];
			snprintf(rate, sizeof(rate), "%6.1f MB/s",
				   (bytes / 1048576.0) / elapsed);
			extra = rate;
		}
		
		AppendWithSpace(&extra, message);
		std::string str = std::string(name.data(), name.size());
		fprintf(stdout, "%-02s : %11.3f micros/op;%s%s\n", str.c_str(),
				seconds * 1e6 / done, (extra.empty() ? "" : " "), extra.c_str());
		if (FLAGS_histogram) {
			fprintf(stdout, "Microseconds per op:\n%s\n", hist.ToString().c_str());
		}
		fflush(stdout);
	}
};

// State shared by all concurrent executions of the same benchmark.
struct SharedState {
	std::mutex mu;
	std::condition_variable cv;
	int total;

	// Each thread goes through the following states:
	//    (1) initializing
	//    (2) waiting for others to be initialized
	//    (3) running
	//    (4) done

	int numinitialized;
	int numdone;
	bool start;

	SharedState(int total)
	  : total(total), numinitialized(0), numdone(0), start(false) {}
};

// Per-thread state for concurrent executions of the same benchmark.
struct ThreadState {
	int tid;      // 0..n-1 when running in n threads
	std::default_random_engine rand;  // Has different seeds for different threads
	Stats stats;
	SharedState* shared;
	ThreadState(int index) : tid(index), rand(1000 + index), shared(nullptr) {}
};

class Benchmark {
public:
	Benchmark() :num(FLAGS_num),
        valuesize(FLAGS_value_size),
        entriesperbatch(1),
        reads(FLAGS_reads < 0 ? FLAGS_num : FLAGS_reads),
        heapcounter(0) {

	}

	struct ThreadArg {
		SharedState* shared;
		std::shared_ptr<ThreadState> thread;
		std::function<void(const std::shared_ptr<ThreadState>&)> method;
	};

	void PrintEnvironment() {
	#if defined(__linux)
		time_t now = time(nullptr);
		fprintf(stderr, "Date:       %s", ctime(&now));  // ctime() adds newline

		FILE* cpuinfo = fopen("/proc/cpuinfo", "r");
		if (cpuinfo != nullptr) {
			char line[1000];
			int numcpus = 0;
			std::string cputype;
			std::string cachesize;
			while (fgets(line, sizeof(line), cpuinfo) != nullptr) {
				const char* sep = strchr(line, ':');
				if (sep == nullptr) {
					continue;
				}
				
				std::string_view key = TrimSpace(std::string_view(line, sep - 1 - line));
				std::string_view val = TrimSpace(std::string_view(sep + 1));
				if (key == "model name") {
					++numcpus;
					cputype = std::string(val.data(), val.size());
				} else if (key == "cache size") {
				  cachesize = std::string(val.data(), val.size());
				}
			}
			
			fclose(cpuinfo);
			fprintf(stderr, "CPU:        %d * %s\n", numcpus, cputype.c_str());
			fprintf(stderr, "CPUCache:   %s\n", cachesize.c_str());
		}
	#endif
	}
	
	void Open() {
		FLAGS_db = "./dbbench";
		Options options;
		options.createifmissing = true;
		options.writebuffersize = FLAGS_write_buffer_size;
		options.blocksize = FLAGS_block_size;
		options.reuselogs = FLAGS_reuse_logs;
		options.env->NewLogger("db_bench_log", options.infolog);
		options.infolog->SetInfoLogLevel(InfoLogLevel::DEBUG_LEVEL);

		db.reset(new DB(options, FLAGS_db));
		Status s = db->Open();
		if (!s.ok()) {
			fprintf(stderr, "open error: %s\n", s.ToString().c_str());
			exit(1);
		}
	}

	void PrintHeader() {
		const int kKeySize = 16;
		PrintEnvironment();
		fprintf(stdout, "Keys:       %d bytes each\n", kKeySize);
		fprintf(stdout, "Values:     %d bytes each (%d bytes after compression)\n",
				FLAGS_value_size,
				static_cast<int>(FLAGS_value_size * FLAGS_compression_ratio + 0.5));
		fprintf(stdout, "Entries:    %d\n", num);
		fprintf(stdout, "RawSize:    %.1f MB (estimated)\n",
				((static_cast<int64_t>(kKeySize + FLAGS_value_size) * num) /
				 1048576.0));
		fprintf(stdout, "filesize:   %.1f MB (estimated)\n",
				(((kKeySize + FLAGS_value_size * FLAGS_compression_ratio) * num) /
				 1048576.0));
		fprintf(stdout, "------------------------------------------------\n");
	}
  
	void Run() {
		PrintHeader();
		Open();
		
		const char* benchmarks = FLAGS_benchmarks;
		while (benchmarks != nullptr) {
			const char* sep = strchr(benchmarks, ',');
			std::string_view name;
			if (sep == nullptr) {
				name = benchmarks;
				benchmarks = nullptr;
			} 
			else {
				name = std::string_view(benchmarks, sep - benchmarks);
				benchmarks = sep + 1;
			}
			
			// Reset parameters that may be overridden below
			num = FLAGS_num;
			reads = (FLAGS_reads < 0 ? FLAGS_num : FLAGS_reads);
			valuesize = FLAGS_value_size;
			entriesperbatch = 1;
			writeoptions = WriteOptions();
			std::function<void(const std::shared_ptr<ThreadState>&)> method;
			bool freshdb = false;
			int numthreads = FLAGS_threads;
			
			if (name == std::string_view("fillseq")) {
				freshdb = true;
				method = std::bind(&Benchmark::WriteSeq, this, std::placeholders::_1);
			}
			else if (name == std::string_view("fillsync")) {
				freshdb = true;
				num /= 1000;
				writeoptions.sync = true;
				method = std::bind(&Benchmark::WriteRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("fillrandom")) {
				freshdb = true;
				method = std::bind(&Benchmark::WriteRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("overwrite"))	{
				freshdb = false;
        method = std::bind(&Benchmark::WriteRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readrandom")) {
        method = std::bind(&Benchmark::ReadRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readseq")) {
        method = std::bind(&Benchmark::ReadSequential, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readreverse")) {
				method = std::bind(&Benchmark::ReadReverse, this, std::placeholders::_1);
			}
			else if (name == std::string_view("compact")) {
				method = std::bind(&Benchmark::Compact, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readrandom")) {
				method = std::bind(&Benchmark::ReadRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readseq")) {
				method = std::bind(&Benchmark::ReadSequential, this, std::placeholders::_1);
			}
			else if (name == std::string_view("readreverse")) {
				method = std::bind(&Benchmark::ReadReverse, this, std::placeholders::_1);
			}
			else if (name == std::string_view("fill100K")) {
				freshdb = true;
				num /= 1000;
				valuesize = 100 * 1000;
				method = std::bind(&Benchmark::WriteRandom, this, std::placeholders::_1);
			}
			else if (name == std::string_view("crc32c")) {
				method = std::bind(&Benchmark::Crc32c, this, std::placeholders::_1);
			}

			if (freshdb) {
				if (FLAGS_use_existing_db) {
					fprintf(stdout, "%-12s : skipped (--use_existing_db is true)\n",
					  name.data());
					method = nullptr;
				}
				else {
					db.reset();
					db->DestroyDB(FLAGS_db, Options());
					Open();
				}
			}

			if (method != nullptr) {
				RunBenchmark(numthreads, name, method);
			}
		}
	}
	
	void RunBenchmark(int n, std::string_view name,
                    std::function<void(const std::shared_ptr<ThreadState>&)> &func) {
		SharedState shared(n);

		std::vector<std::shared_ptr<ThreadArg>> args;
		for (int i = 0; i < n; i++) {
			std::shared_ptr<ThreadArg> arg(new ThreadArg());
			arg->method = func;
			arg->shared = &shared;
			arg->thread.reset(new ThreadState(i));
			arg->thread->shared = &shared;
			args.push_back(arg);
			std::thread newthread(std::bind(&Benchmark::ThreadBody, this, arg));
			newthread.detach();
		}

		std::unique_lock<std::mutex> lk(shared.mu);
		while (shared.numinitialized < n) {
			shared.cv.wait(lk);
		}

		shared.start = true;
		shared.cv.notify_all();
		while (shared.numdone < n) {
			shared.cv.wait(lk);
		}

		for (int i = 1; i < n; i++) {
			args[0]->thread->stats.Merge(args[i]->thread->stats);
		}
		args[0]->thread->stats.Report(name);
	}

	void ThreadBody(const std::shared_ptr<ThreadArg> &arg) {
		SharedState* shared = arg->shared;
		std::shared_ptr<ThreadState> thread = arg->thread;
		{
			std::unique_lock<std::mutex> lk(shared->mu);
			shared->numinitialized++;
			if (shared->numinitialized >= shared->total) {
				shared->cv.notify_all();
			}
			
			while (!shared->start) {
				shared->cv.wait(lk);
			}
		}

		thread->stats.Start();
		arg->method(thread);
    	thread->stats.Stop();

		{
			std::unique_lock<std::mutex> lk(shared->mu);
			shared->numdone++;
			if (shared->numdone >= shared->total) {
				shared->cv.notify_all();
			}
		}
	}	


	void Crc32c(const std::shared_ptr<ThreadState>& thread) {
    // Checksum about 500MB of data total
    const int size = 4096;
    const char* label = "(4K per op)";
    std::string data(size, 'x');
    int64_t bytes = 0;
    uint32_t crc = 0;
    while (bytes < 500 * 1048576) {
      crc = crc32c::Value(data.data(), size);
      thread->stats.FinishedSingleOp();
      bytes += size;
    }
    // Print so result is not dead
    fprintf(stderr, "... crc=0x%x\r", static_cast<unsigned int>(crc));

    thread->stats.AddBytes(bytes);
    thread->stats.AddMessage(label);
  }

	void SnappyUncompress(const std::shared_ptr<ThreadState>& thread) {
    RandomGenerator gen;
    std::string_view input = gen.Generate(Options().blocksize);
    std::string compressed;
    bool ok = Snappy_Compress(input.data(), input.size(), &compressed);
    int64_t bytes = 0;
    char* uncompressed = (char*)malloc(input.size());
    while (ok && bytes < 1024 * 1048576) {  // Compress 1G
      ok = Snappy_Uncompress(compressed.data(), compressed.size(),
                                   uncompressed);
      bytes += input.size();
      thread->stats.FinishedSingleOp();
    }
    free(uncompressed);

    if (!ok) {
      thread->stats.AddMessage("(snappy failure)");
    } else {
      thread->stats.AddBytes(bytes);
    }
  }

	void SnappyCompress(const std::shared_ptr<ThreadState>& thread) {
		RandomGenerator gen;
		std::string_view input = gen.Generate(Options().blocksize);
		int64_t bytes = 0;
		int64_t produced = 0;
		bool ok = true;
		std::string compressed;
		while (ok && bytes < 1024 * 1048576) {  // Compress 1G
		  ok = Snappy_Compress(input.data(), input.size(), &compressed);
		  produced += compressed.size();
		  bytes += input.size();
		  thread->stats.FinishedSingleOp();
		}

		if (!ok) {
		  thread->stats.AddMessage("(snappy failure)");
		} else {
		  char buf[100];
		  snprintf(buf, sizeof(buf), "(output: %.1f%%)",
				   (produced * 100.0) / bytes);
		  thread->stats.AddMessage(buf);
		  thread->stats.AddBytes(bytes);
		}
	}

	void ReadReverse(const std::shared_ptr<ThreadState>& thread) {
		std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
		int i = 0;
		int64_t bytes = 0;
		for (iter->SeekToLast(); i < reads && iter->Valid(); iter->Prev()) {
			bytes += iter->key().size() + iter->value().size();
			thread->stats.FinishedSingleOp();
			++i;
		}
		thread->stats.AddBytes(bytes);
	}

	void Compact(const std::shared_ptr<ThreadState>& thread) {
		db->CompactRange(nullptr, nullptr); 
	}

	void ReadSequential(const std::shared_ptr<ThreadState>& thread) {
		std::shared_ptr<Iterator> iter = db->NewIterator(ReadOptions());
		int i = 0;
		int64_t bytes = 0;
		for (iter->SeekToFirst(); i < reads && iter->Valid(); iter->Next()) {
			bytes += iter->key().size() + iter->value().size();
			thread->stats.FinishedSingleOp();
			++i;
		}
		thread->stats.AddBytes(bytes);
	}

	void ReadRandom(const std::shared_ptr<ThreadState>& thread) {
		ReadOptions options;
		std::string value;
    int found = 0;
    for (int i = 0; i < reads; i++) {
      char key[100];
      const int k = thread->rand() % FLAGS_num;
      snprintf(key, sizeof(key), "%016d", k);
      if (db->Get(options, key, &value).ok()) {
        found++;
      }
      thread->stats.FinishedSingleOp();
    }
    char msg[100];
    snprintf(msg, sizeof(msg), "(%d of %d found)", found, num);
    thread->stats.AddMessage(msg);
	} 

	void WriteRandom(const std::shared_ptr<ThreadState>& thread) {
		DoWrite(thread, false); 
	}

	void WriteSeq(const std::shared_ptr<ThreadState>& thread) {
		DoWrite(thread, true); 
	}
	
	void DoWrite(const std::shared_ptr<ThreadState>& thread, bool seq) {
		if (num != FLAGS_num) {
			char msg[100];
			snprintf(msg, sizeof(msg), "(%d ops)", num);
			thread->stats.AddMessage(msg);
		}

		RandomGenerator gen;
		WriteBatch batch;
		Status s;
		int64_t bytes = 0;
		for (int i = 0; i < num; i += entriesperbatch) {
			batch.clear();
			for (int j = 0; j < entriesperbatch; j++) {
				const int k = seq ? i + j : (thread->rand() % FLAGS_num);
				char key[100];
				snprintf(key, sizeof(key), "%016d", k);
				batch.Put(key, gen.Generate(valuesize));
				bytes += valuesize + strlen(key);
				thread->stats.FinishedSingleOp();
			}

			s = db->Write(writeoptions, &batch);
			if (!s.ok()) {
				fprintf(stderr, "put error: %s\n", s.ToString().c_str());
				exit(1);
			}
		}
		thread->stats.AddBytes(bytes);
	}

private:
	std::shared_ptr<DB> db;
	int num;
	int valuesize;
	int entriesperbatch;
	WriteOptions writeoptions;
	int reads;
	int heapcounter;
	Options options;
};

int main()
{
	Benchmark benchmark;
	benchmark.Run();
	return 0;
}
