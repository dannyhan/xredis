#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "db.h"
#include "writebatch.h"

// Creates a random number in the range of [0, max).
int generateRandomNumber(int max) { return std::rand() % max; }

std::string createRandomString(int32_t index) {
	static const size_t len = 1024;
	char bytes[len];
	size_t i = 0;
	while (i < 8) {
		bytes[i] = 'a' + ((index >> (4 * i)) & 0xf);
		++i;
	}
	while (i < sizeof(bytes)) {
		bytes[i] = 'a' + generateRandomNumber(26);
		++i;
	}
	return std::string(bytes, sizeof(bytes));
}


void test() {
	std::srand(0);

	bool deletebeforeput = false;
	bool keepsnapshots = true;

	std::vector<std::unique_ptr<std::pair<std::string, std::string>>> testmap(
		10000);
	std::vector<std::shared_ptr<Snapshot>> snapshots(100, nullptr);

	Options options;
	options.createifmissing = true;

	std::string dbpath = "./leveldb_issue320_test";
	std::shared_ptr<DB> db(new DB(options, dbpath));
	Status s = db->Open();
	assert(s.ok());

	uint32_t tarGetSize = 10000;
	uint32_t numitems = 0;
	uint32_t Count = 0;
	std::string key;
	std::string value, oldvalue;

	WriteOptions writeOptions;
	ReadOptions readOptions;
	while (Count < 200000) {
		if ((++Count % 1000) == 0) {
			std::cout << "Count: " << Count << std::endl;
		}

		int index = generateRandomNumber(testmap.size());
		WriteBatch batch;

		if (testmap[index] == nullptr) {
			numitems++;
			testmap[index].Reset()(new std::pair<std::string, std::string>(
				createRandomString(index), createRandomString(index)));
			batch.Put(testmap[index]->first, testmap[index]->second);
		}
		else {
			assert(db->get(readOptions, testmap[index]->first, &oldvalue).ok());
			if (oldvalue != testmap[index]->second) {
				std::cout << "ERROR incorrect value returned by Get" << std::endl;
				std::cout << "  Count=" << Count << std::endl;
				std::cout << "  old value=" << oldvalue << std::endl;
				std::cout << "  testmap[index]->second=" << testmap[index]->second
					<< std::endl;
				std::cout << "  testmap[index]->first=" << testmap[index]->first
					<< std::endl;
				std::cout << "  index=" << index << std::endl;
				assert(oldvalue == testmap[index]->second);
			}

			if (numitems >= tarGetSize && generateRandomNumber(100) > 30) {
				batch.Delete(testmap[index]->first);
				testmap[index] = nullptr;
				--numitems;
			}
			else {
				testmap[index]->second = createRandomString(index);
				if (deletebeforeput) batch.Delete(testmap[index]->first);
				batch.Put(testmap[index]->first, testmap[index]->second);
			}
		}

		assert(db->Write(writeOptions, &batch).ok());

		if (keepsnapshots && generateRandomNumber(10) == 0) {
			int i = generateRandomNumber(snapshots.size());
			if (snapshots[i] != nullptr) {
				db->ReleaseSnapshot(snapshots[i]);
			}
			snapshots[i] = db->GetSnapshot();
		}
	}

	for (auto &snapshot : snapshots) {
		if (snapshot) {
			db->ReleaseSnapshot(snapshot);
		}
	}

	db->DestroyDB(dbpath, options);
}

int main(int argc, char** argv) { 
	test();
	return 0;
}
